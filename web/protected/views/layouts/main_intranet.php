<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="icon" type="image/png" href="<?=Yii::app()->request->baseUrl?>/img/icon.png">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/fontawesome-free/css/all.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/toastr/toastr.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <?php if($this->id=='horarios'){ ?>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/jquery-timepicker/jquery.timepicker.min.css">
    <?php } ?>
    <?php if($this->id=='turnos' || $this->id=='estadistica'){ ?>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/daterangepicker/daterangepicker.css">
    <?php } ?>
    
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/dist/css/adminlte.min.css">

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css?t=<?=time()?>">
    <script> var urlSite = '<?=Yii::app()->request->baseUrl?>';</script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">
		<?php Yii::app()->runController('header/index'); ?>
		<?php Yii::app()->runController('menu/index'); ?>
    	<?php echo $content; ?>

		<?php Yii::app()->runController('footer/index'); ?>
	</div>
	<!-- ./wrapper -->
    <?php $this->renderPartial('//usuarios/modal_reset_password');?>
<!-- jQuery -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Toastr -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/toastr/toastr.min.js"></script>
<!-- Select2 -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<?php if($this->id=='horarios'){ ?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/jquery-timepicker/jquery.timepicker.min.js"></script>
<?php } ?>
<?php if($this->id=='turnos' || $this->id=='estadistica'){ ?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/moment/moment.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<?php } ?>   
<!-- AdminLTE App -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/dist/js/adminlte.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>
<?php if($this->id=='usuarios'){ ?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/usuarios.js"></script>
<?php } ?>
<?php if($this->id=='horarios'){ ?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/horarios.js"></script>
<?php } ?>
<?php if($this->id=='turnos'){ ?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/turnos.js"></script>
<?php } ?>
<?php if($this->id=='reservas'){ ?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/reservas.js"></script>
<?php } ?>
<?php if($this->id=='estadistica'){ ?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/estadistica.js?t=<?=time()?>"></script>
<?php } ?>
</body>
</html>
