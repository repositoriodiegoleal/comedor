<?php

class MenuController extends Controller
{
	public function actionIndex()
	{
		$permisos = Yii::app()->user->getPermisos();
		$permisosCustom = array();
		foreach($permisos as $p){
			$permiso = Permisos::model()->findByPk($p->idpermiso);
			if(!empty($permiso) && $permiso->nombre != 'reservas-offline'){
				switch($permiso->nombre){
					case 'estadistica': $permisoNombre = 'estadística'; break;
					default: $permisoNombre = $permiso->nombre;
				}
				array_push($permisosCustom, array('id'=>ucwords($permiso->id), 'titulo'=>ucwords($permisoNombre), 'url'=>$permiso->nombre));
			}
		}
		sort($permisosCustom);
		$this->render('index', array('permisos'=>$permisosCustom));
	}

}