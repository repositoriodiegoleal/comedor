<main id="main">
    <!-- ======= Services Section ======= -->
    <section class="control-ingreso">
		
		<?php
		$form = $this->beginWidget('CActiveForm',array(
			'method' =>'POST',
			'action'=> Yii::app()->createUrl('ControlIngresoAlumno/Validar'),
			'enableClientValidation'=>true,
			'id'=>'idform',
			'enableAjaxValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
				'validateOnChange' => true,
                'validateOnType' => true,
			),
		));
		?>
		<div class="container">
			<div class="section-title">
				<h2>CONTROL DE INGRESO</h2>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<h4 class="title">INGRESOS POR TURNO</h4>
					<table class="table table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>HORARIOS</th>
								<th>ASISTENCIAS</th>
							</tr>
						</thead>
						<tbody id="bodycargadatos">
							<?php
								$listado = BusqReservas::buscarReservas();
								//echo '<br>';print_r(json_encode($listado));die;
								for($i=0;$i<count($listado);$i++){
									echo "<tr><td>".$listado[$i]['horainiciofin']."</td><td class='cantidad-ingreso'>".$listado[$i]['cantidad']."</td></tr>";
								}
							?>
						</tbody>
						<tfoot>
							<tr>
								<th>TOTAL</th>
								<th><input type="text" id="total-ingresos" class="form-control" readonly/></th>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="col-lg-8">
					<table class="table table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>INGRESA EL CÓDIGO O DNI DE LA RESERVA</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div>
										<?php echo $form->labelEx($model,'codigo');?>
										<?php echo $form->textField($model,'codigo', array('class'=>'form-control', 'placeholder'=>'Ingresa el código de la reserva','onclick'=>"js:this.value='';document.getElementById('ValidarIngreso_dni_em_').innerHTML = '';document.getElementById('ValidarIngreso_codigo_em_').innerHTML = '';document.getElementById('ValidarIngreso_dni').value='';"));?>
										<div><span class="comentario">(Debes hacer click en el campo para poder borrar)</span></div>
									</div>
									<div class="d-flex align-items-center">
										<div class="box-response mt-3"><?php echo $form->error($model,'codigo',array('style'=>'font-size:20px; text-align:center'));?></div>
									</div>
								</td>
								
							</tr>
							<tr>
								<td>
									<div>
										<?php echo $form->labelEx($model,'dni');?>
										<?php echo $form->textField($model,'dni', array('class'=>'form-control', 'placeholder'=>'Ingresa el DNI del alumno','onclick'=>"js:this.value='';document.getElementById('ValidarIngreso_codigo_em_').innerHTML = '';document.getElementById('ValidarIngreso_dni_em_').innerHTML = '';document.getElementById('ValidarIngreso_codigo').value='';"));?>
										<div><span class="comentario">(Debes hacer click en el campo para poder borrar)</span></div>
									</div>
									<div class="d-flex align-items-center">
										<div class="box-response mt-3">
											<?php echo $form->error($model,'dni',array('style'=>'font-size:20px; text-align:center'));?>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>

			
		</div>	
		<?php $this->endWidget();?>

	</section><!-- End Services Section -->
</main><!-- End #main -->

<script>
	function cargartabla(){}
</script>
