<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Agregar</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/inicio">Inicio</a></li>
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/reservas">Reservas</a></li>
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/reservas/ver/<?=$modelturno->id?>">Listado</a></li>
              <li class="breadcrumb-item active">Agregar</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <?php if(!empty($responseReserva) && $responseReserva['status']=='error'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-exclamation-triangle"></i>Atención</h5>
                        <?=$responseReserva['msg']?>
                    </div>
                </div>
            </div>
            <?php }else if(!empty($responseReserva) && $responseReserva['status']=='ok'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-check"></i>Perfecto</h5>
                        <?=$responseReserva['msg']?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Reserva</h3>
                        </div><!-- /.card-header -->
                        <?php 
                            $form=$this->beginWidget('CActiveForm', array(
                                //'action'=>Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/login',
                                //'method'=>'post',
                                'id'=>'reserva-form',
                                'enableAjaxValidation'=>false,
                                //'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                            )); 
                        ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($model,'email'); ?>
                                            <?php echo $form->textField($model,'email',array('class'=>'form-control')); ?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($reservaOffline){ ?>
                                    <div class="col-sm-2">
                                        <label for="tipo">Tipo</label>
                                        <select class="form-control" name="Reservas[tipo]" id="tipo">
                                            <option value="online">Online</option>
                                            <option value="offline">Offline</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group dni-group" style="display:none">
                                            <label for="dni">DNI</label>
                                            <textarea rows="1" class="form-control" name="Reservas[dni]" id="Reservas_dni"></textarea>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($model,'detalle'); ?>
                                            <?php echo $form->textArea($model,'detalle',array('rows'=>3,'class'=>'form-control')); ?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary btn-guardar')); ?>
                                <?php echo CHtml::link('Cancelar', Yii::app()->request->baseUrl.'/reservas/ver/'.$modelturno->id , array('class'=>'btn btn-default')); ?>
                            </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- /.card -->
                </div>
            </div> <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section><!-- /.content -->
</div>
<script>
    var action = '<?=Yii::app()->controller->action->id?>';
</script>