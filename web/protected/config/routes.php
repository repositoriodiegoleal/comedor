<?php

return array(
    'urlFormat'=>'path',
    'caseSensitive'=>false,
    'rules'=>array(
        
        // Example route with parameters
        // 'usuarios/ver/<id>'=>'users/view/id/<id>',

        'login'=>'intranet/login',
        'logout'=>'intranet/logout',
        'inicio'=>'intranet/index',

        // USUARIOS
        'usuarios/editar/<id>'=>'usuarios/editar/id/<id>',
        'usuarios/eliminar/<id>'=>'usuarios/eliminar/id/<id>',

        // HORARIOS
        'horarios/editar/<id>'=>'horarios/editar/id/<id>',
        'horarios/eliminar/<id>'=>'horarios/eliminar/id/<id>',

        // TURNOS
        'turnos/editar/<id>'=>'turnos/editar/id/<id>',
        'turnos/eliminar/<id>'=>'turnos/eliminar/id/<id>',

        // RESERVAS
        'reservas/ver/<id>'=>'reservas/ver/id/<id>',
        'reservas/agregar/<id>'=>'reservas/agregar/id/<id>',
        'reservas/eliminar/<id>'=>'reservas/eliminar/id/<id>',

        // ELIMINACION DE LA RESERVA
        //'eliminarreserva/eliminar/<token>'=>'eliminarreserva/eliminar/token/<token>',
        'cancelar/reserva/<token>'=>'eliminarreserva/eliminar/token/<token>',

        //PANTALLA DE CONTROL DE INGRESO DE ALUMNO AL COMEDOR
        'controlingreso'=>'controlingresoalumno/validar',


        // Default
        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
    ),
);