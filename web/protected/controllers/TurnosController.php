<?php

class TurnosController extends Controller
{
	public $layout='main_intranet';

	public function filters(){
		return array('accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules(){
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				//'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            )	
        );
	}

	public function actionIndex()
	{
		$turnos = Turnos::model()->findAll(array('condition'=>'estado!=:estado','params'=>array('estado'=>'E')));
		$this->render('index', array('turnos'=>$turnos));
	}

	public function actionActualizarEstado()
	{
		if(!empty($_POST['idTurno'])){
			$turno = Turnos::model()->findByPk($_POST['idTurno']);
			$turno->estado = $_POST['estado'];
			if($turno->update()){
				echo json_encode(array(
					"status"=>"ok",
					"msg"=>"Estado actualizado."
					));
			}else{
				echo json_encode(array(
					"status"=>"error",
					"msg"=>"No se pudo actualizar el estado."
					));
			}
		}
	}

	public function actionEditar($id)
	{
		$model = Turnos::model()->findByPk($id);
		if($model->estado == 'E')
			$this->redirect('../../turnos');
		
		if(isset($_POST['Turnos']))
		{
			$_POST['Turnos']['estado'] = isset($_POST['Turnos']['estado']) && $_POST['Turnos']['estado'] == 'on' ? '1':'0';
			//$_POST['Turnos']['fecha'] = Utils::fechaBD($_POST['Turnos']['fecha']);
			$_POST['Turnos']['activodesde'] = Utils::fechaBD($_POST['Turnos']['activodesde']);
			$_POST['Turnos']['activohasta'] = Utils::fechaBD($_POST['Turnos']['activohasta']);
			$model->attributes=$_POST['Turnos'];
			if($model->save()){
				$this->redirect('../../turnos');
			}
		}

		// Modifico el formato de las fechas para el calendario.
		$model->fecha = Utils::fechaWeb($model->fecha);
		$model->activodesde = Utils::fechaWeb($model->activodesde);
		$model->activohasta = Utils::fechaWeb($model->activohasta);
		
		$this->render('form',array(
			'model'=>$model,
		));
	}

	public function actionAgregar()
	{
		if(isset($_POST['Turnos']))
		{ 
			$_POST['Turnos']['estado'] = isset($_POST['Turnos']['estado']) && $_POST['Turnos']['estado'] == 'on' ? '1':'0';
			$fechas = explode(' - ',$_POST['Turnos']['fecha']);
			$desde = Utils::fechaBD($fechas[0]);
			$hasta = Utils::fechaBD($fechas[1]);
			$activoDesde = Utils::fechaBD($_POST['Turnos']['activodesde']);
			$activohasta = Utils::fechaBD($_POST['Turnos']['activohasta']);
			if($_POST['Turnos']['horario'] == 'all'){
				$horarios = Horarios::model()->findAll(array('condition'=>'estado=:estado','params'=>array('estado'=>'1')));
			}else{
				$horarios = Horarios::model()->findAll(array('condition'=>'estado=:estado AND id=:id','params'=>array('estado'=>'1','id'=>$_POST['Turnos']['horario'] )));
			}
			
			$idHorarios = array();
			foreach($horarios as $horario){
				$idHorarios[$horario->dia][] = $horario->id;
			}

			$inicio = new DateTime($desde);
			$fin   = new DateTime($hasta);
			$inicioActivo = new DateTime($activoDesde);
			$finActivo   = new DateTime($activohasta);

			for($fecha = $inicio; $fecha <= $fin; $fecha->modify('+1 day')){
				$dia = Utils::diaWeb($fecha->format("D"));
				if(array_key_exists($dia, $idHorarios)){
					foreach($idHorarios[$dia] as $id){
						$model = new Turnos;
						$model->fecha = $fecha->format("Y-m-d");
						$model->cupo = $_POST['Turnos']['cupo'];
						$model->activodesde = $inicioActivo->format("Y-m-d");
						$model->activohasta = $finActivo->format("Y-m-d");
						$model->idhorario = $id;
						$model->estado = $_POST['Turnos']['estado'];
						$model->save();
					}
				}
			}
			$this->redirect(array('index'));
			
		}

		$this->render('form_publicar');
	}

	public function actionEliminar($id)
	{ 
		// Turnos::model()->findByPk($id)->delete();
		$model = Turnos::model()->findByPk($id);
		$model->estado = 'E';
		$model->update();
		// Elimino las reservas del turno
		$reservas = Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado!=:estado','params'=>array('idturno'=>$model->id, 'estado'=>'E')));
		foreach($reservas as $reserva){
			$reserva->estado = 'E';
			$reserva->update();
		}
		$this->redirect('../../turnos');	
	}
	
}