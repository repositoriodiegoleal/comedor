<div class="modal fade" id="modal-eliminar-horario">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Atención</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>¿Estás seguro de eliminar el horario?</p>
            <p class="text-gray">*Si eliminas el horario, se eliminarán los turnos y reservas asociados a él.</p>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <a href="#" class="btn btn-primary" id="btn-continuar-horario">Continuar</a>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->