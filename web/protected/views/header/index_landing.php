<?php //echo 'header';

?>
<!-- ======= Top Bar ======= -->
<div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-google-map"></i> <?=Yii::app()->params['direccionSbu']?>
      </div>
      <div class="contact-info ml-auto">
        <i class="icofont-envelope"></i> <a href="mailto:<?=Yii::app()->params['emailSbu']?>"><?=Yii::app()->params['emailSbu']?></a>
        <i class="icofont-phone"></i> <?=Yii::app()->params['telefonoSbu']?>
      </div>
      <!-- <div class="social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a>
      </div> -->
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="<?=Yii::app()->request->baseUrl?>">Comedor Universitario</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      

      <a href="<?=Yii::app()->request->baseUrl?>/login" target="_blank" class="appointment-btn scrollto">Intranet</a>

    </div>
  </header><!-- End Header -->