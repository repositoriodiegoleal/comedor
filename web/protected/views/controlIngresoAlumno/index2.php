<?php echo "
	<html>
	<head>
  		<title>".Yii::app()->params['nombreOrigenCorreo']."</title>
	</head>
	<body>
  		<table  style='width: 100%;'>
			<caption style='background-color: rgb(29, 95, 138);'><h1><font color='White'>".Yii::app()->params['nombreOrigenCorreo']."</font></h1></caption>  
    		<tr>
      			<th>
				    <td><h2>
				  		Contacto de correo: ".Yii::app()->params['emailSbu']."
				    </h2></td>
					<td><h2>
						Contacto de telefono: ".Yii::app()->params['telefonoSbu']."
				    </h2></td>
					<td><h2>
						Dirección: ".Yii::app()->params['direccionSbu']."
				    </h2></td>
				</th>
    		</tr>
    	</table>"?>
		
		<?php
		$form = $this->beginWidget('CActiveForm',array(
			'method' =>'POST',
			'action'=> Yii::app()->createUrl('ControlIngresoAlumno/Validar'),
			'enableClientValidation'=>true,
			'id'=>'idform',
			'enableAjaxValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
				'validateOnChange' => true,
                'validateOnType' => true,
			),
		));
		?>
		<div class="container" style="height: 100px;background-color: rgb(222, 241, 251);">
		<table style="width: 100%;float:none;text-align: center;">
			<tr>
				<td>
					<caption>
					<h1>
						<?php echo $form->error($model,'codigo',array('style'=>'font-size:30px; text-align:center'));?>
						<?php echo $form->error($model,'dni',array('style'=>'font-size:30px; text-align:center'));?>
					</h1>
					</caption>  
				</td>
			</tr>
		</table>
		</div>
		<table style="width: 100%;float:none;text-align: center;">
			<tr>
				<td>
					<br>
					<?php echo $form->labelEx($model,'codigo',array('style'=>'font-size:30px; text-align:center'));?>
					<?php echo $form->textField($model,'codigo', array('style'=>'font-size:30px; text-align:center','onclick'=>"js:this.value='';"));?>
				</td>
			<tr>
			<tr>
				<td>
					Debe realizar click en el control de codigo para poder borrar.
				</td>
			</tr>	
		</table>
		<table style="width: 100%;float:none;text-align: center;">
			<tr>
				<td>
					<br>
					<?php echo $form->labelEx($model,'dni',array('style'=>'font-size:30px; text-align:center'));?>
					<?php echo $form->textField($model,'dni', array('style'=>'font-size:30px; text-align:center','onclick'=>"js:this.value='';"));?>
				</td>
			<tr>
			<tr>
				<td>
					Debe realizar click en el control de codigo para poder borrar.
				</td>
			</tr>	
		</table>

		<?php echo CHtml::submitButton('SALIR',array('onclick'=>"js:this->render('login', array('usuarios'=>null, 'permisos'=>null));"));?>
		<?php $this->endWidget();?>
	</body>
</html>