<?php

class IntranetController extends Controller
{
	public $layout='main_intranet';

	public function filters(){
		return array('accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules(){
		return array(
			array('allow',
                'actions'=>array('login'),
            ),
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				//'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            )	
        );
	}

	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		$permisos = Yii::app()->user->getPermisos();
		//echo '<pre>';print_r($permisos);print_r($permisos[0]->idpermiso);die; 
		if(count($permisos)==1 && $permisos[0]->idpermiso==8){
			BusqReservas::borrarTabla();
			$this->redirect('controlingreso');
		}else{
			$this->render('index');
		}
	}

	public function actionLogin()
	{
		$this->layout='main_page';
		$model=new LoginForm;
		$error = null;
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			if($model->login()){ 
				$this->redirect('inicio');
			} else {
				$error = CActiveForm::validate($model);
			}	
		}
		
		// display the login form
		$this->render('login',array('model'=>$model, 'error'=>$error));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect('login');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}