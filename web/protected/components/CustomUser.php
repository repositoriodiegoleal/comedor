<?php

class CustomUser extends CWebUser {

    private $model;

	function getName(){
		$user = $this->loadUser(Yii::app()->user->id);
		return $user->nombre.' '.$user->apellido;
	}

    function getPermisos(){
		$user = $this->loadUser(Yii::app()->user->id);
		return $user->permisos;
	}

    private function loadUser($id = null)
	{
		if($this->model === null)
		{
			if($id !== null){
				 $this->model = Usuarios::model()->findByPk($id);
			}
		}
		return $this->model;
	}
}

?>