<main id="main">
    <!-- ======= Services Section ======= -->
    <section class="cancelar-reserva">
        <div class="container">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="card card-primary" id="modal-consulta">
                        <div class="card-header">
                            <h4 class="card-title">CANCELAR RESERVA</h4>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="hidden" id="token" value="<?=!empty($token)?$token:''?>"/>
                                        <p class="msg-cancelar-reserva">¿Estás seguro de eliminar tu reserva?</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="<?=Yii::app()->request->baseUrl?>" class="btn btn-default">Cancelar</a>
                            <button id="cancelar-reserva" class="btn btn-primary float-right">Continuar</button>
                        </div>
                    </div>
                    <div class="card card-primary" id="modal-respuesta" style="display:none">
                        <div class="card-header">
                            <h4 class="card-title">ATENCIÓN</h4>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <p class="msg-cancelar-reserva"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="<?=Yii::app()->request->baseUrl?>" class="btn btn-primary float-right">Volver al sitio</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </section><!-- End Services Section -->
</main><!-- End #main -->
<!-- jQuery -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/jquery/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        console.log('vista cancelar reserva');
        $(document).on('click','#cancelar-reserva',function(e) {
            e.preventDefault();
            cancelarReserva();
        });
    });

    function cancelarReserva(){
        token = $('#token').val();
        $.ajax({
            url: urlSite+'/eliminarreserva/cancelar',
            data: {
                token:token
            },
            type: 'post',
            chache: false,
            beforeSend: function() {
                //openLoading();
            },
        })
        .done(function (response) {
            console.log(response);
            var obj = JSON.parse(response);
            $('#modal-consulta').hide();
            $('#modal-respuesta').show();
            $('#modal-respuesta .msg-cancelar-reserva').text(obj.message);
        })
        .fail(function(jqXHR, textStatus, errorThrown ) {
            console.log('error guardar ficha',textStatus,errorThrown);
            //showMessage('error', 'ERROR', 'Se produjo un error inesperado. Contacte con el administrador.');
        })
        .always(function() {
            //closeLoading();
        });
    }

</script>

