$(document).ready(function() {
    console.log('init turnos');
    
    activeMenuItem('turnos');

    initTablaHorarios();

    $(document).on('change','.chk-estado',function() {
        actualizarEstado(this);
    });

    $(document).on('click','.btn-eliminar',function(e) {
        e.preventDefault();
        $("#btn-continuar-turno").attr("href", $(this).attr('href'));
    });

    initCalendarios();
    $('.select2').select2();
});

function initTablaHorarios(){
    window.tablaHorarios = $('#admin-turnos').DataTable({
        //"dom": '<"top"i>rt<"bottom"flp><"clear">',
        //"dom": '<"myfilter"f><"mylength"l>t<"myinfo"i><"mypaginate"p><"clear">',
        //"dom": '<"myfilter"f>t<"myinfo"i><"mypaginate"p><"clear">',
        //"dom": 't<"myinfo"i><"mypaginate"p><"clear">',
        "responsive": { details: false},
        "order": [[ 1, "asc" ]],
        "columnDefs":[{
            "targets": [0,5],
            "orderable":false
        }],
        "language": {
            "processing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron resultados",
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "0 registros encontrados",
            "infoFiltered": "(filtrado de un total de _MAX_ registro/s)",
            "search": "Buscar:",
            "infoThousands": ",",
            "loadingRecords": "Cargando...",
            "paginate": {
                "previous": "Siguiente",
                "next": "Anterior",
                "first": "Primera",
                "last": "Ultima"
            },
            "info": "_TOTAL_ registro/s encontrado/s"
        },
        "columns": [
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 1, className: "mobile-p mobile-l tablet-p"},
        ],
        "initComplete": function () {
            
        }
    });
}

function actualizarEstado(input){
    let idTurno = $(input).attr('id').replace('chk-desktop-','');
    let estado = $(input).is(':checked') == true ? '1':'0';
    $.ajax({
        url: urlSite+'/turnos/actualizarestado',
        data: {
            idTurno:idTurno,
            estado:estado
        },
        type: 'post',
        chache: false,
        beforeSend: function() {
            //console.log('antes de enviar la solicitud');
            openLoading();
        },
    })
    .done(function (response) {
        console.log(response);
        var obj = JSON.parse(response);
        if (obj.status === 'ok') {
            showMessage('success', 'OK', 'Se ha actualizado el estado.');
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown ) {
        console.log('Error update-estado:',textStatus,errorThrown);
        showMessage('error', 'ERROR', 'Se produjo un error inesperado. Contacte con el administrador.');
    })
    .always(function() {
        //console.log('despues de enviar la solicitud');
        closeLoading();
    });
}

function initCalendarios(){
    $('.single-calendar').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Aceptar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizar",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        // "startDate": "2016-01-01",
        // "endDate": "2016-01-07",
        singleDatePicker: true,
        "minDate": moment(),
        "opens": "center",
        // "maxSpan": {
        //     "days": 7
        // },
        "autoApply": true
    });

    $('.calendar').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Aceptar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizar",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        // "startDate": "2016-01-01",
        // "endDate": "2016-01-07",
        "minDate": moment(),
        "opens": "center",
        // "maxSpan": {
        //     "days": 7
        // },
        "autoApply": true
    });

    // $('.single-calendar').on('apply.daterangepicker', function(ev, picker) {
    //     $(this).val(picker.startDate.format('DD/MM/YYYY'));
    // });

    // $('.single-calendar').trigger('apply.daterangepicker');
}

