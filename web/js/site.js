$(document).ready(function() {
    console.log('init site');

    $('.owl-carousel').owlCarousel({
        loop:true,
        autoplay:true,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })

    $('.single-calendar').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Aceptar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizar",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        //"startDate": "desde 2016-01-01",
        // "endDate": "2016-01-07",
        singleDatePicker: true,
        "minDate": moment(),
        "opens": "center",
        // "maxSpan": {
        //     "days": 7
        // },
        "autoApply": true
    });

    // $('.single-calendar').on('apply.daterangepicker', function(ev, picker) {
    //     $(this).val('Desde '+picker.startDate.format('DD/MM/YYYY'));
    // });

    actualizarTurnos();

    $(document).on('click','#btn-buscar',function(e) {
        e.preventDefault();
        actualizarTurnos();
    });
    
    $(document).on('click','.btn-reserva-cliente',function(e) {
        e.preventDefault();
        $('#idTurno').val($(this).data("id"));
    });

    $(document).on('click','#btn-confirmar-reserva',function(e) {
        e.preventDefault();
        let form = document.querySelector('#confirmacion-reserva-form')
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            form.classList.add('was-validated');
        }else{
            console.log('formulario validado');
        }
        if($('#email').val() == ''){
            console.log("email inválido");
            return false;
        }
        reservarTurno($('#idTurno').val(),$('#email').val());
    });

    $('#modal-reserva-turno').on('hidden.bs.modal', function (e) {
        $('#email').val('');
        let form = document.querySelector('#confirmacion-reserva-form');
        if(typeof form !== 'undefined'){
            form.reset();
            form.classList.remove('was-validated');
        }
    });

    $(document).on('click','#box-error .close',function(e) {
        e.preventDefault();
        $("#box-error").hide();
    });

    $(document).on('click','#box-ok .close',function(e) {
        e.preventDefault();
        $("#box-ok").hide();
    });
});

function initTablaTurnos(){
    window.tablaHorarios = $('#turnos').DataTable({
        "pageLength": 50,
        "searching": false,
        "lengthChange": false,
        "info": false,
        "responsive": { details: false},
        "order": [[ 0, "asc" ]],
        "language": {
            "processing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ turnos",
            "zeroRecords": "No se encontraron resultados",
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "0 turnos encontrados",
            "infoFiltered": "(filtrado de un total de _MAX_ turno/s)",
            "search": "Buscar:",
            "infoThousands": ",",
            "loadingRecords": "Cargando...",
            "paginate": {
                "previous": "Siguiente",
                "next": "Anterior",
                "first": "Primera",
                "last": "Ultima"
            },
            "info": "_TOTAL_ turno/s encontrado/s"
        },
        "columns": [
            { responsivePriority: 1, className: "tablet-l desktop"}
        ],
        "initComplete": function () {
            
        }
    });
}

function actualizarTurnos(){
    fechaCalendario = $('#calendario').val();
    $.ajax({
        url: urlSite+'/site/turnosdisponibles',
        data: {
            fecha:fechaCalendario
        },
        type: 'post',
        chache: false,
        beforeSend: function() {
            //openLoading();
        },
    })
    .done(function (response) {
        var obj = JSON.parse(response);
        $('#turnos').DataTable().destroy();
        $('#turnos tbody tr').remove();
        if (obj.status === 'ok') {
            let items = obj.data;
            if(items.length > 0){
                items.forEach(function(item, key){
                    $('#turnos tbody').append(htmlSeparadorDia(item));
                    item.turnos.forEach(function(turno, key){
                        if(turno.disponible || verTurnoNoDisponible == 'si'){
                            $('#turnos tbody').append(htmlRowTurno(turno));
                        }
                    });
                });
                
            }else{
                $('#turnos tbody').append(`<tr class=""><td><p class="align-middle text-center"><strong>No se encontraron turnos para la fecha seleccionada</strong></p></td></tr>`);
            }
        }
        initTablaTurnos();
    })
    .fail(function(jqXHR, textStatus, errorThrown ) {
        console.log('error guardar ficha',textStatus,errorThrown);
        //showMessage('error', 'ERROR', 'Se produjo un error inesperado. Contacte con el administrador.');
    })
    .always(function() {
        //closeLoading();
    });
}

function diaLocal(dia){
    switch(dia){
        case 'LUN': dia = 'Lun'; break;
        case 'MAR': dia = 'Mar'; break;
        case 'MIE': dia = 'Mie'; break;
        case 'JUE': dia = 'Jue'; break;
        case 'VIE': dia = 'Vie'; break;
        case 'SAB': dia = 'Sab'; break;
        case 'DOM': dia = 'Dom'; break;
    }
    return dia;
}

function ordenDia(dia){
    switch(dia){
        case 'LUN': dia = '1'; break;
        case 'MAR': dia = '2'; break;
        case 'MIE': dia = '3'; break;
        case 'JUE': dia = '4'; break;
        case 'VIE': dia = '5'; break;
        case 'SAB': dia = '6'; break;
        case 'DOM': dia = '7'; break;
    }
    return dia;
}

function htmlSeparadorDia(item){
    return `
    <tr>
        <td class="separador-dia" data-order="${item.fecha}-${ordenDia(item.dia)}">${item.fechaString}</td>
    </tr>
    `;
}

function htmlRowTurno(item){
    let btnReservar = '';
    let msgDisponible = '';
    if(item.disponible){
        btnReservar = `<button type="button" data-id="${item.id}" class="btn btn-success btn-reserva-cliente" data-toggle="modal" data-target="#modal-reserva-turno">Reservar</button>`;
        msgDisponible = 'Disponible';
    }else{
        msgDisponible = 'No disponible';
    }
    return `
    <tr class="row-turno-desktop">
        <td class="turno-dia" data-order="${item.fecha}-${ordenDia(item.horario.dia)}-${ordenDia(item.horario.horainicio)}">
            <div class="row">
                <div class="col-md-4 text-center text-md-left mt-2 mt-md-0 mb-2 mb-md-0">${diaLocal(item.horario.dia)} ${item.fecha.split('-')[2]} de ${item.horario.horainicio} a ${item.horario.horafin}</div>
                <div class="col-md-4 text-center mt-2 mt-md-0 mb-2 mb-md-0">${msgDisponible}</div>
                <div class="col-md-4 text-center mt-2 mt-md-0 mb-2 mb-md-0">${btnReservar}</div>
            </div>
        </td>
    </tr>
    `;
}

function reservarTurno(idTurno, email){
    $.ajax({
        url: urlSite+'/site/reservar',
        data: {
            idturno:idTurno,
            email:email
        },
        type: 'post',
        chache: false,
        beforeSend: function() {
            //openLoading();
        },
    })
    .done(function (response) {
        $("#box-error").hide();
        $("#box-ok").hide();
        console.log(response);
        var obj = JSON.parse(response);
        if (obj.status === 'ok') {
            $("#box-ok").show();
        }else{
            $("#box-error").show();
            $("#detalle-error").html(obj.message);
        }
        actualizarTurnos()
        $('#modal-reserva-turno').modal('hide');
    })
    .fail(function(jqXHR, textStatus, errorThrown ) {
        console.log('error reservar turno',textStatus,errorThrown);
        //showMessage('error', 'ERROR', 'Se produjo un error inesperado. Contacte con el administrador.');
    })
    .always(function() {
        //closeLoading();
    });
}