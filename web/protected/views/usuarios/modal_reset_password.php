<div class="modal fade" id="modal-reset-password">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cambiar contraseña</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-reset-password">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="password-1" class="required">Nueva contraseña *</label>
                                <input type="text" class="form-control" id="password-1" required/>
                                <div class="invalid-feedback">
                                    Complete el campo.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="password-2" class="required">Confirmar contraseña *</label>
                                <input type="text" class="form-control" id="password-2" required/>
                                <div class="invalid-feedback">
                                    Complete el campo.
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a href="#" class="btn btn-primary" id="btn-reset-password">Continuar</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->