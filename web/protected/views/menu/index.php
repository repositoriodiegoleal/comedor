<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=Yii::app()->request->baseUrl?>/inicio" class="brand-link">
      <img src="<?=Yii::app()->request->baseUrl?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Comedor</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?=Yii::app()->request->baseUrl?>/img/user.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?=Yii::app()->user->getName()?></a>
        </div>
      </div>

      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          
          <li class="nav-header">INTRANET</li>

          <?php if(!empty($permisos)){ ?>
            <?php foreach($permisos as $permiso){ ?>
              <li class="nav-item">
                <a href="<?=Yii::app()->request->baseUrl.'/'.$permiso['url']?>" class="nav-link nav-link-<?=$permiso['url']?>">
                  <?php
                    $icon = '';
                    switch($permiso['url']){
                      case 'horarios': $icon = 'fa fa-hourglass-start'; break;
                      case 'turnos': $icon = 'far fa-calendar-alt'; break;
                      case 'reservas': $icon = 'far fa-calendar-check'; break;
                      case 'usuarios': $icon = 'fa fa-users'; break;
                      case 'estadistica': $icon = 'fas fa-chart-line'; break;
                    }
                  ?>
                  <i class="nav-icon <?=$icon?>"></i>
                  <p><?=$permiso['titulo']?></p>
                </a>
              </li>
            <?php } ?>
          <?php } ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>