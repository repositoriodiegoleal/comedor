<?php

class ValidarIngreso extends CFormModel{
    public $codigo;
    public $dni;

    public function rules(){
        return  [
                    ['codigo,dni','required','message'=>' '],
                    ['codigo,dni','numerical', 'integerOnly'=>true,'message'=>'Debe ingresar numeros'],
                    ['codigo','length', 'max'=>22],
                    ['codigo','verificarcodigo'],
                    ['dni','length','max'=>9],
                    ['dni','ext.verificardni'],
                ];
    }
    public function verificarcodigo($attribute,$params){
        if(strlen($this->$attribute)==22 || strlen($this->$attribute)==21){
            $result = Utils::controlIngresoAlumno($this->$attribute);
            $this->addError('codigo',$result['message']);
        }else{
            if(strlen($this->$attribute)!=0){
                $this->addError('codigo','Digitos ingresados:'.strlen($this->codigo));
            }else{
                $this->addError('codigo',' ');
            }
        }    
    }
}
?>