-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 10-04-2022 a las 20:49:33
-- Versión del servidor: 5.7.26-log
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `comedorfull`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE IF NOT EXISTS `alumnos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `dni` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id`, `nombre`, `apellido`, `email`, `dni`) VALUES
(1, 'alumno', 'alumno', 'usuariodiegoleal@gmail.com', '12345678');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

DROP TABLE IF EXISTS `horarios`;
CREATE TABLE IF NOT EXISTS `horarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `horainicio` time NOT NULL,
  `horafin` time NOT NULL,
  `dia` varchar(3) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

DROP TABLE IF EXISTS `permisos`;
CREATE TABLE IF NOT EXISTS `permisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `nombre`, `estado`) VALUES
(1, 'horarios', '1'),
(2, 'turnos', '1'),
(3, 'reservas', '1'),
(4, 'usuarios', '1'),
(7, 'reservas-offline', '1'),
(8, 'Entrada Comedor', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

DROP TABLE IF EXISTS `reservas`;
CREATE TABLE IF NOT EXISTS `reservas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `detalle` varchar(255) NOT NULL,
  `token` text NOT NULL,
  `idturno` int(11) NOT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reservas_fk_1` (`idturno`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tacademico`
--

DROP TABLE IF EXISTS `tacademico`;
CREATE TABLE IF NOT EXISTS `tacademico` (
  `idacademico` int(11) NOT NULL AUTO_INCREMENT,
  `idalumno` int(11) NOT NULL,
  `idfacultad` int(11) NOT NULL,
  `idcarrera` int(11) NOT NULL,
  `ingreso` int(4) NOT NULL,
  `lu` varchar(10) COLLATE utf16_spanish_ci NOT NULL,
  `mataprob` varchar(2) COLLATE utf16_spanish_ci NOT NULL,
  `matregul` varchar(2) COLLATE utf16_spanish_ci NOT NULL,
  `asislunes` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `asismartes` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `asismiercoles` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `asisjueves` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `asisviernes` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `poseebeca` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `nombbeca` varchar(50) COLLATE utf16_spanish_ci NOT NULL,
  `direccionlectivo` varchar(50) COLLATE utf16_spanish_ci NOT NULL,
  `barriolectivo` varchar(20) COLLATE utf16_spanish_ci NOT NULL,
  `dondecursa` varchar(16) COLLATE utf16_spanish_ci NOT NULL,
  `tipoalu` int(11) NOT NULL,
  PRIMARY KEY (`idacademico`),
  KEY `idalumno` (`idalumno`),
  KEY `idfacultad` (`idfacultad`),
  KEY `idcarrera` (`idcarrera`)
) ENGINE=InnoDB AUTO_INCREMENT=2751 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talumno`
--

DROP TABLE IF EXISTS `talumno`;
CREATE TABLE IF NOT EXISTS `talumno` (
  `idalumno` int(11) NOT NULL AUTO_INCREMENT,
  `apellido` varchar(30) COLLATE utf16_spanish_ci NOT NULL,
  `nombre` varchar(40) COLLATE utf16_spanish_ci NOT NULL,
  `dni` varchar(11) COLLATE utf16_spanish_ci NOT NULL,
  `fechnac` date NOT NULL,
  `correo` varchar(40) COLLATE utf16_spanish_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf16_spanish_ci NOT NULL,
  `celular` varchar(16) COLLATE utf16_spanish_ci NOT NULL,
  `trabajo` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `trabajoblanc` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `cantpervive` varchar(2) COLLATE utf16_spanish_ci NOT NULL,
  `ingrfami` varchar(10) COLLATE utf16_spanish_ci NOT NULL,
  `direcorigen` varchar(100) COLLATE utf16_spanish_ci NOT NULL,
  `localidadorigen` int(11) NOT NULL,
  `sexo` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `esperahijo` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `canthijos` varchar(2) COLLATE utf16_spanish_ci NOT NULL,
  `conhijos` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `obrasocial` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `nombobrasocial` varchar(30) COLLATE utf16_spanish_ci NOT NULL,
  `vegetariano` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `celiaco` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `alukey` varchar(60) COLLATE utf16_spanish_ci NOT NULL,
  PRIMARY KEY (`idalumno`),
  KEY `sexo` (`sexo`),
  KEY `localidadorigen` (`localidadorigen`)
) ENGINE=InnoDB AUTO_INCREMENT=2817 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tasisteen`
--

DROP TABLE IF EXISTS `tasisteen`;
CREATE TABLE IF NOT EXISTS `tasisteen` (
  `idasisteen` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(17) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idasisteen`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tasisteen`
--

INSERT INTO `tasisteen` (`idasisteen`, `nombre`) VALUES
(3, 'S. S. de Jujuy'),
(4, 'Sede de San Pedro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tasistencia`
--

DROP TABLE IF EXISTS `tasistencia`;
CREATE TABLE IF NOT EXISTS `tasistencia` (
  `idasistencia` int(11) NOT NULL AUTO_INCREMENT,
  `idalumno` int(11) NOT NULL,
  `fechasistencia` date NOT NULL,
  PRIMARY KEY (`idasistencia`),
  KEY `idalumno` (`idalumno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tcarrera`
--

DROP TABLE IF EXISTS `tcarrera`;
CREATE TABLE IF NOT EXISTS `tcarrera` (
  `idcarrera` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf16_spanish_ci NOT NULL,
  `idfacultad` int(11) NOT NULL,
  `sede` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  PRIMARY KEY (`idcarrera`),
  KEY `idfacultad` (`idfacultad`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `tcarrera`
--

INSERT INTO `tcarrera` (`idcarrera`, `nombre`, `idfacultad`, `sede`) VALUES
(1, 'sin definir', 5, ''),
(118, 'Ingeniería Química', 1, '1'),
(119, 'Ingeniería de Minas', 1, '1'),
(120, 'Ingeniería Industrial', 1, '1'),
(121, 'Ingeniería Informática', 1, '1'),
(122, 'Licenciatura en Sistemas', 1, '1'),
(123, 'Licenciatura en Tecnología de los Alimentos', 1, '1'),
(124, 'Licenciatura en Ciencias Geológicas', 1, '1'),
(125, 'Analista Programador Universitario', 1, '1'),
(126, 'Tecnicatura Universitaria en Ciencias de la Tierra', 1, '1'),
(127, 'Técnico Universitario en Procesamiento de Minerales', 1, '1'),
(128, 'Técnico Universitario en Explotación de Minas', 1, '1'),
(129, 'Tecnicatura Universitaria en Perforaciones', 1, '1'),
(130, 'Tecnicatura Universitaria en Ciencias de la Tierra Orientada a Petróleo', 1, '1'),
(131, 'Tecnicatura Universitaria En Diseño Integral De Videojuegos', 1, '1'),
(132, 'Analista Programador Universitario', 1, '2'),
(133, 'Ingeniería Agronómica', 2, '1'),
(134, 'Licenciatura en Bromatología', 2, '1'),
(135, 'Bromatología', 2, '1'),
(136, 'Licenciatura en Ciencias Biológicas', 2, '1'),
(137, 'Ingeniería Agronómicas', 2, '2'),
(138, 'Tecnicatura Universitaria Forestal', 2, '2'),
(139, 'Contador Público', 3, '1'),
(140, 'Licenciatura en Administración', 3, '1'),
(141, 'Licenciatura en Economía Política', 3, '1'),
(142, 'Licenciatura en Antropologia', 4, '1'),
(143, 'Licenciatura en Ciencias de la Educación', 4, '1'),
(144, 'Profesorado en Ciencias de la Educación', 4, '1'),
(145, 'Licenciatura en Comunicación Social', 4, '1'),
(146, 'Licenciatura en Educación para la Salud', 4, '1'),
(147, 'Profesorado en Educación para la Salud', 4, '1'),
(148, 'Licenciatura en Letras', 4, '1'),
(149, 'Profesorado en Letras', 4, '1'),
(150, 'Licenciatura en Trabajo Social', 4, '1'),
(151, 'Licenciatura en Filosofía', 4, '1'),
(152, 'Profesorado en Filosofía', 4, '1'),
(153, 'Licenciatura en Historia', 4, '1'),
(154, 'Profesorado en Historia', 4, '1'),
(155, 'Tecnicatura en Comunicación Digital Convergente', 4, '1'),
(156, 'Licenciatura en Ciencias de la Educación', 4, '2'),
(157, 'Profesorado en Ciencias de la Educación', 4, '2'),
(158, 'Licenciatura en Educación para la Salud', 4, '2'),
(159, 'Técnico Minero', 5, '1'),
(160, 'Técnico Químico', 5, '1'),
(161, 'Técnico en Informática', 5, '1'),
(163, 'Licenciatura en Ciencia Política', 6, '1'),
(164, 'Enfermería Universitaria', 7, '1'),
(165, 'Ciclo Básico', 5, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tevaluador`
--

DROP TABLE IF EXISTS `tevaluador`;
CREATE TABLE IF NOT EXISTS `tevaluador` (
  `idatencion` int(11) NOT NULL AUTO_INCREMENT,
  `idalumno` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `observacion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `observacionrecepcion` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idatencion`)
) ENGINE=InnoDB AUTO_INCREMENT=740 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tfacultad`
--

DROP TABLE IF EXISTS `tfacultad`;
CREATE TABLE IF NOT EXISTS `tfacultad` (
  `idfacultad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf16_spanish_ci NOT NULL,
  `sede` varchar(3) COLLATE utf16_spanish_ci NOT NULL,
  PRIMARY KEY (`idfacultad`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `tfacultad`
--

INSERT INTO `tfacultad` (`idfacultad`, `nombre`, `sede`) VALUES
(1, 'Facultad de Ingenieria', 'all'),
(2, 'Facultad de Ciencias Agrarias', 'all'),
(3, 'Facultad de Ciencias Economicas', '1'),
(4, 'Facultad de Humanidades y Ciencias Sociales', 'all'),
(5, 'Escuela de Minas', '1'),
(6, 'Escuela Superior de Ciencias Jurídicas y Políticas', '1'),
(7, 'Escuela Superior de Ciencias de la Salud', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `thistorialevaluador`
--

DROP TABLE IF EXISTS `thistorialevaluador`;
CREATE TABLE IF NOT EXISTS `thistorialevaluador` (
  `idatencion` int(11) NOT NULL AUTO_INCREMENT,
  `idalumno` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `observacion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `observacionrecepcion` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idatencion`)
) ENGINE=InnoDB AUTO_INCREMENT=952 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `thistorialrecepcion`
--

DROP TABLE IF EXISTS `thistorialrecepcion`;
CREATE TABLE IF NOT EXISTS `thistorialrecepcion` (
  `idhistorialrecepcion` int(11) NOT NULL AUTO_INCREMENT,
  `idalumno` int(11) NOT NULL,
  `fechrecepcion` date NOT NULL,
  `forminscripcion` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `copidni` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `certinegatividad` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `copirecibo` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `copiservicio` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `certianalitico` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `copilibretaminas` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fotautennivmedio` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `certaluregular` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `estado` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  PRIMARY KEY (`idhistorialrecepcion`),
  KEY `idalumno` (`idalumno`),
  KEY `idusuario` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=860 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tlocalidad`
--

DROP TABLE IF EXISTS `tlocalidad`;
CREATE TABLE IF NOT EXISTS `tlocalidad` (
  `idlocalidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) COLLATE utf16_spanish_ci NOT NULL,
  PRIMARY KEY (`idlocalidad`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `tlocalidad`
--

INSERT INTO `tlocalidad` (`idlocalidad`, `nombre`) VALUES
(1, 'Abra Pampa'),
(2, 'A. Castro Tolay'),
(3, 'Abralaite'),
(4, 'P. del Marques'),
(5, 'S.S. de Jujuy'),
(6, 'Yala'),
(7, 'El Carmen'),
(8, 'Aguas Calientes'),
(9, 'Monterrico'),
(10, 'Pampa Blanca'),
(11, 'Perico'),
(12, 'Puesto Viejo'),
(13, 'Humahuaca'),
(14, 'El Aguilar'),
(15, 'Hipolito Yrigoyen'),
(16, 'Tres Cruces'),
(17, 'L. Gral. San Martin'),
(18, 'Caimancito'),
(19, 'Calilegua'),
(20, 'Fraile Pintado'),
(21, 'Yuto'),
(22, 'Palpala'),
(23, 'Rinconada'),
(24, 'Mina Pirquitas'),
(25, 'San Antonio'),
(26, 'S. Pedro de Jujuy'),
(27, 'Arrayanal'),
(28, 'Barrio Negro'),
(29, 'El Piquete'),
(30, 'La Esperanza'),
(31, 'La Mendieta'),
(32, 'Rodeito'),
(33, 'Santa Clara'),
(34, 'El Fuerte'),
(35, 'El Talar'),
(36, 'Palma Sola'),
(37, 'Vinalito'),
(38, 'Santa Catalina'),
(39, 'Cieneguillas'),
(40, 'Cusi Cusi'),
(41, 'Susques'),
(42, 'Catua'),
(43, 'Coranzuli'),
(44, 'Tilcara'),
(45, 'Huacalera'),
(46, 'Maimara'),
(47, 'Tumbaya'),
(48, 'Purmamarca'),
(49, 'Volcan'),
(50, 'Valle Grande'),
(51, 'Caspala'),
(52, 'Pampichuela'),
(53, 'San Francisco'),
(54, 'Santa Ana'),
(55, 'La Quiaca'),
(56, 'Barrios'),
(57, 'Cangrejillos'),
(58, 'El Condor'),
(59, 'Pumahuasi'),
(60, 'Yavi'),
(61, 'Prob. Buenos Aires'),
(62, 'Prov. Catamarca'),
(63, 'Prov. Chaco'),
(64, 'Prov. Chubut'),
(65, 'Prov. Cordoba'),
(66, 'Prov. Corrientes'),
(67, 'Prov. Entre Rios'),
(68, 'Prov. Formosa'),
(69, 'Prov.  La Pampa'),
(70, 'Prov.  La Rioja'),
(71, 'Prov. Mendoza'),
(72, 'Prov. Misiones'),
(73, 'Prov. Neuquen'),
(74, 'Prov. Rio Negro'),
(75, 'Prov. Salta'),
(76, 'Prov. San Juan'),
(77, 'Prov. San Luis'),
(78, 'Prov. Santa Cruz'),
(79, 'Prov. Santa Fe'),
(80, 'Prov. Santiago del Estero'),
(81, 'Prov. Tierra del Fuego'),
(82, 'Prov. Tucuman');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trecepcion`
--

DROP TABLE IF EXISTS `trecepcion`;
CREATE TABLE IF NOT EXISTS `trecepcion` (
  `idrecepcion` int(11) NOT NULL AUTO_INCREMENT,
  `idalumno` int(11) NOT NULL,
  `fechrecepcion` date NOT NULL,
  `forminscripcion` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `copidni` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `certinegatividad` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `copirecibo` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `copiservicio` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `certianalitico` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `copilibretaminas` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fotautennivmedio` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `certaluregular` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `estado` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `bloqueo` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  PRIMARY KEY (`idrecepcion`),
  KEY `idalumno` (`idalumno`),
  KEY `idusuario` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=697 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turnos`
--

DROP TABLE IF EXISTS `turnos`;
CREATE TABLE IF NOT EXISTS `turnos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `cupo` int(11) NOT NULL,
  `activodesde` date NOT NULL,
  `activohasta` date NOT NULL,
  `idhorario` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `turnos_fk_1` (`idhorario`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tusuario`
--

DROP TABLE IF EXISTS `tusuario`;
CREATE TABLE IF NOT EXISTS `tusuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `apellido` varchar(30) COLLATE utf16_spanish_ci NOT NULL,
  `nombre` varchar(40) COLLATE utf16_spanish_ci NOT NULL,
  `dni` varchar(11) COLLATE utf16_spanish_ci NOT NULL,
  `celular` varchar(16) COLLATE utf16_spanish_ci NOT NULL,
  `mail` varchar(40) COLLATE utf16_spanish_ci NOT NULL,
  `nombusu` varchar(15) COLLATE utf16_spanish_ci NOT NULL,
  `clave` varchar(16) COLLATE utf16_spanish_ci NOT NULL,
  `tipousuario` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  `eliminar` varchar(1) COLLATE utf16_spanish_ci NOT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `tusuario`
--

INSERT INTO `tusuario` (`idusuario`, `apellido`, `nombre`, `dni`, `celular`, `mail`, `nombusu`, `clave`, `tipousuario`, `eliminar`) VALUES
(1, 'nadie', 'nadie', '', '', '', '@!_!28402LFG', '@@_284522+-!!', '', '0'),
(9, 'SIVILA', 'MAURICIO', '', '', '', 'msivila', '1117', '3', '0'),
(10, 'RIOS', 'JOSE', '', '', '', 'jrios', '2806', '1', '1'),
(11, 'FERNANDEZ', 'JUAN', '', '', '', 'jfernandez', '2624', '1', '1'),
(12, 'VALDIVIEZO', 'CRISTIAN', '', '', '', 'cvaldiviezo', '1234', '1', '1'),
(13, 'AISAMA', 'ARIEL', '', '', '', 'aaisama', '9491', '1', '1'),
(14, 'DELGADO', 'LUCIANA', '', '', '', 'ldelgado', 'lupejoa', '2', '1'),
(15, 'SALAZAR', 'SEBASTIAN', '', '', '', 'ssalazar', '2345', '1', '1'),
(16, 'PERALTA', 'ADRIANA', '', '', '', 'aperalta', '4567', '1', '1'),
(17, 'MARCIAL', 'PATRICIA', '', '', '', 'pmarcial', '6108', '1', '1'),
(18, 'MARCIAL', 'PATRICIA', '', '', '', 'pmarcial', '6810', '1', '1'),
(19, 'TORREJON', 'FERNANDO', '', '', '', 'ftorrejon', '28|5', '3', '1'),
(20, 'TORREJON', 'FERNANDO', '', '', '', 'ftorrejon', '8619', '3', '1'),
(21, 'SOTO', 'YANINA', '', '', '', 'ysoto', '3578', '1', '1'),
(22, 'SOTO', 'YANINA', '', '', '', 'ysoto', '3578', '2', '1'),
(23, 'HERRERA', 'NOEMI', '', '', '', 'nherrera', '9875', '2', '1'),
(24, 'WAYAR', 'FABIANA', '', '', '', 'fwayar', '1357', '1', '1'),
(25, 'FLORES', 'ROXANA', '', '', '', 'rflores', '3478', '1', '1'),
(26, 'FLORES', 'ROXANA', '', '', '', 'rflores', '3478', '1', '1'),
(27, 'REYES', 'SILVIA', '', '', '', 'sreyes', '6789', '1', '1'),
(28, '', '', '', '', '', 'ftorrejon', '123456', '3', '0'),
(29, '', '', '', '', '', 'recepcion1', '1234', '1', '0'),
(30, '', '', '', '', '', 'evaluador1', '1234', '2', '0'),
(31, '', '', '', '', '', 'sreyes', '14787336', '1', '0'),
(32, '', '', '', '', '', 'vortega', 'votega76', '1', '1'),
(33, '', '', '', '', '', 'vortega', '1976', '1', '0'),
(34, '', '', '', '', '', 'ldelgado', '301819', '2', '0'),
(35, '', '', '', '', '', 'ysoto', '123456', '2', '0'),
(36, '', '', '', '', '', 'ssalazar', '123456', '1', '0'),
(37, '', '', '', '', '', 'gbissoti', '123456', '1', '0'),
(38, '', '', '', '', '', 'jfernandez', '123456', '1', '0'),
(39, '', '', '', '', '', 'fwayar', '123456', '1', '0'),
(40, '', '', '', '', '', 'aperalta', '123456', '1', '0'),
(41, '', '', '', '', '', 'rflores', '123456', '1', '0'),
(42, '', '', '', '', '', 'pmarcial', '123456', '1', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariopermisos`
--

DROP TABLE IF EXISTS `usuariopermisos`;
CREATE TABLE IF NOT EXISTS `usuariopermisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) NOT NULL,
  `idpermiso` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuarios_fk_1` (`idusuario`),
  KEY `permisos_fk_2` (`idpermiso`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuariopermisos`
--

INSERT INTO `usuariopermisos` (`id`, `idusuario`, `idpermiso`) VALUES
(65, 1, 1),
(66, 1, 2),
(67, 1, 3),
(68, 1, 4),
(69, 1, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `password`, `nombre`, `apellido`, `email`, `telefono`, `estado`) VALUES
(1, 'admin@admin.com', '123456', 'Pepe', 'Argento', '', '', '1');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD CONSTRAINT `reservas_fk_1` FOREIGN KEY (`idturno`) REFERENCES `turnos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `talumno`
--
ALTER TABLE `talumno`
  ADD CONSTRAINT `talumno_ibfk_1` FOREIGN KEY (`localidadorigen`) REFERENCES `tlocalidad` (`idlocalidad`);

--
-- Filtros para la tabla `tasistencia`
--
ALTER TABLE `tasistencia`
  ADD CONSTRAINT `tasistencia_ibfk_1` FOREIGN KEY (`idalumno`) REFERENCES `talumno` (`idalumno`);

--
-- Filtros para la tabla `tcarrera`
--
ALTER TABLE `tcarrera`
  ADD CONSTRAINT `tcarrera_ibfk_1` FOREIGN KEY (`idfacultad`) REFERENCES `tfacultad` (`idfacultad`);

--
-- Filtros para la tabla `trecepcion`
--
ALTER TABLE `trecepcion`
  ADD CONSTRAINT `trecepcion_ibfk_1` FOREIGN KEY (`idalumno`) REFERENCES `talumno` (`idalumno`),
  ADD CONSTRAINT `trecepcion_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `tusuario` (`idusuario`);

--
-- Filtros para la tabla `turnos`
--
ALTER TABLE `turnos`
  ADD CONSTRAINT `turnos_fk_1` FOREIGN KEY (`idhorario`) REFERENCES `horarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuariopermisos`
--
ALTER TABLE `usuariopermisos`
  ADD CONSTRAINT `permisos_fk_2` FOREIGN KEY (`idpermiso`) REFERENCES `permisos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_fk_1` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
