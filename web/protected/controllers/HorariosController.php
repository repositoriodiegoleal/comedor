<?php

class HorariosController extends Controller
{
	public $layout='main_intranet';

	public function filters(){
		return array('accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules(){
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				//'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            )	
        );
	}

	public function actionIndex()
	{
		$horarios = Horarios::model()->findAll(array('condition'=>'estado!=:estado','params'=>array('estado'=>'E')));
		$this->render('index', array('horarios'=>$horarios));
	}

	public function actionActualizarEstado()
	{
		if(!empty($_POST['idHorario'])){
			$horario = Horarios::model()->findByPk($_POST['idHorario']);
			$horario->estado = $_POST['estado'];
			if($horario->update()){
				echo json_encode(array(
					"status"=>"ok",
					"msg"=>"Estado actualizado."
					));
			}else{
				echo json_encode(array(
					"status"=>"error",
					"msg"=>"No se pudo actualizar el estado."
					));
			}
		}
	}

	public function actionEliminar($id)
	{ 
		// Horarios::model()->findByPk($id)->delete();
		$model = Horarios::model()->findByPk($id);
		$model->estado = 'E';
		$model->update();
		// Elimino los turnos asociados al horario
		$turnos = Turnos::model()->findAll(array('condition'=>'idhorario=:idhorario AND estado!=:estado','params'=>array('idhorario'=>$id, 'estado'=>'E')));
		foreach($turnos as $turno){
			$turno->estado = 'E';
			$turno->update();
			// Elimino las reservas del turno
			$reservas = Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado!=:estado','params'=>array('idturno'=>$turno->id, 'estado'=>'E')));
			foreach($reservas as $reserva){
				$reserva->estado = 'E';
				$reserva->update();
			}
		}
		$this->redirect('../../horarios');	
	}

	public function actionEditar($id)
	{
		$model = Horarios::model()->findByPk($id);
		if($model->estado == 'E')
			$this->redirect('../../horarios');
		
		if(isset($_POST['Horarios']))
		{
			$_POST['Horarios']['estado'] = isset($_POST['Horarios']['estado']) && $_POST['Horarios']['estado'] == 'on' ? '1':'0';
			$model->attributes=$_POST['Horarios'];
			if($model->save()){
				$this->redirect('../../horarios');
			}
		}
		
		$this->render('form',array(
			'model'=>$model,
		));
	}

	public function actionAgregar()
	{
		$model = new Horarios;

		if(isset($_POST['Horarios']))
		{//echo '<pre>';print_r($_POST);die;
			$_POST['Horarios']['estado'] = isset($_POST['Horarios']['estado']) && $_POST['Horarios']['estado'] == 'on' ? '1':'0';
			$model->attributes=$_POST['Horarios'];
			if($model->save()){
				$this->redirect(array('index'));
			}
		}

		$this->render('form',array(
			'model'=>$model,
		));
	}

}