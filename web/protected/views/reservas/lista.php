<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Listado</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/inicio">Inicio</a></li>
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/reservas">Reservas</a></li>
              <li class="breadcrumb-item active">Listado</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?=$model->horario->dia.' '.$model->fecha?> <p class="text-horario text-detail"><?='de '.$model->horario->horainicio.' a '.$model->horario->horafin?></p></h3> 
                <?php echo CHtml::link('Agregar', Yii::app()->request->baseUrl.'/reservas/agregar/'.$model->id , array('class'=>'btn btn-primary float-right')); ?>
              </div><!-- /.card-header -->
              <div class="card-body">
                <table id="admin-reservas-detalle" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <tr>
                      <th>Email</th>
                      <th>Token</th>
                      <th>Acciones</th>
                      <th>Reservas</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($reservas)){ ?>
                      	<?php foreach($reservas as $reserva){ ?>
                          <tr>
                            <td data-order="<?=$reserva->email?>">
                              <span><?=$reserva->email?></span>
                              <p class="text-horario text-detail"></p>
                            </td>
                            <td><?=$reserva->token?></td>
                            <td>
                              <a href="<?=Yii::app()->request->baseUrl.'/reservas/eliminar/'.$reserva->id?>" class="btn btn-default btn-eliminar" data-toggle="modal" data-target="#modal-eliminar-reserva"><i class="fas fa-trash-alt"></i></a>
                            </td>
                            <td>
                            <div class="box-mobile">
                              <div class="box-body">
                              <div class="box-item"><?=$reserva->email?></div>
                              <div class="box-item"><?=$reserva->token?></div>
                              </div>
                              <div class="box-body">
                              <div class="box-item"><?=$reserva->detalle?></div>
                              <div class="box-item">Acciones</div>
                              </div>
                            </div>
                            </td>
                        	</tr>
                        <?php }// foreach usuarios ?>
                      <?php }// if usuarios ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
		  
          </div>
          
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<?php require "modal_eliminar.php"; ?>