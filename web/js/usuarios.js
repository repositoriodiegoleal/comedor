$(document).ready(function() {
    console.log('init usuarios');
    
    activeMenuItem('usuarios');

    initTablaUsuarios();

    $(document).on('change','.chk-estado',function() {
        actualizarEstado(this);
    });

    $(document).on('click','.btn-eliminar',function(e) {
        e.preventDefault();
        $("#btn-continuar").attr("href", $(this).attr('href'));
    });

    // FORM
    // Cargo permisos
    if(typeof action != 'undefined' && action == 'editar' && permisosUsuario.length > 0){
        for (var i = 0; i < permisosUsuario.length; i++){
            $('#Usuarios_permisos > option[value='+permisosUsuario[i]+']').attr('selected', 'selected');
        }
    }
    $('.select2').select2();

});

function initTablaUsuarios(){
    window.tablaUsuarios = $('#admin-usuarios').DataTable({
        //"dom": '<"top"i>rt<"bottom"flp><"clear">',
        //"dom": '<"myfilter"f><"mylength"l>t<"myinfo"i><"mypaginate"p><"clear">',
        //"dom": '<"myfilter"f>t<"myinfo"i><"mypaginate"p><"clear">',
        //"dom": 't<"myinfo"i><"mypaginate"p><"clear">',
        "responsive": { details: false},
        "order": [[ 1, "desc" ]],
        "columnDefs":[{
            "targets": [0,4],
            "orderable":false
        }],
        "language": {
            "processing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron resultados",
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "0 registros encontrados",
            "infoFiltered": "(filtrado de un total de _MAX_ registro/s)",
            "search": "Buscar:",
            "infoThousands": ",",
            "loadingRecords": "Cargando...",
            "paginate": {
                "previous": "Siguiente",
                "next": "Anterior",
                "first": "Primera",
                "last": "Ultima"
            },
            "info": "_TOTAL_ registro/s encontrado/s"
        },
        "columns": [
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 1, className: "mobile-p mobile-l tablet-p"},
        ],
        "initComplete": function () {
            
        }
    });
}

function actualizarEstado(input){
    let idUsuario = $(input).attr('id').replace('chk-desktop-','');
    let estado = $(input).is(':checked') == true ? '1':'0';
    $.ajax({
        url: urlSite+'/usuarios/actualizarestado',
        data: {
            idUsuario:idUsuario,
            estado:estado
        },
        type: 'post',
        chache: false,
        beforeSend: function() {
            //console.log('antes de enviar la solicitud');
            openLoading();
        },
    })
    .done(function (response) {
        console.log(response);
        var obj = JSON.parse(response);
        if (obj.status === 'ok') {
            showMessage('success', 'OK', 'Se ha actualizado el estado.');
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown ) {
        console.log('Error update-estado:',textStatus,errorThrown);
        showMessage('error', 'ERROR', 'Se produjo un error inesperado. Contacte con el administrador.');
    })
    .always(function() {
        //console.log('despues de enviar la solicitud');
        closeLoading();
    });
}

