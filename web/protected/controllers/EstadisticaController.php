<?php

class EstadisticaController extends Controller
{
	public $layout='main_intranet';

	public function filters(){
		return array('accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules(){
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				//'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            )	
        );
	}

	public function actionIndex()
	{
		$reservaOffline = false;
		$permisos = Yii::app()->user->getPermisos();
		foreach($permisos as $p){
			$permiso = Permisos::model()->findByPk($p->idpermiso);
			if(!empty($permiso) && $permiso->nombre === 'reservas-offline'){
				$reservaOffline = true;
			}
		}
		$horarios = Horarios::model()->findAll(array('condition'=>'estado!=:estado','params'=>array('estado'=>'E')));
		$this->render('index', array('horarios'=>$horarios, 'reservaOffline'=>$reservaOffline));
	}

	public function actionBuscar()
	{
		if(!empty($_POST['fecha']) && !empty($_POST['idhorario'])){
			$fechas = explode(" - ", $_POST['fecha']);
			$desde = Utils::fechaBD($fechas[0]);
			$hasta = Utils::fechaBD($fechas[1]);
			$data = array();
			$inicio = new DateTime($desde);
			$fin   = new DateTime($hasta);
			for($fecha = $inicio; $fecha <= $fin; $fecha->modify('+1 day')){
				if($_POST['idhorario'] == 'all'){
					$turnos = Turnos::model()->findAll(array('condition'=>'fecha=:fecha AND estado!=:estado','params'=>array('fecha'=>$fecha->format("Y-m-d"), 'estado'=>'E')));
				}else{
					$turnos = Turnos::model()->findAll(array('condition'=>'fecha=:fecha AND idhorario=:idhorario AND estado!=:estado','params'=>array('fecha'=>$fecha->format("Y-m-d"),'idhorario'=>$_POST['idhorario'], 'estado'=>'E')));
				}
				$cupoPorDia = 0;
				$reservasTotal = 0;
				$reservasNoConsumidas = 0;
				$reservasConsumidas = 0;
				$reservasTotalOffline = 0;
				$reservasNoConsumidasOffline = 0;
				$reservasConsumidasOffline = 0;
				$reservasTotalOnline = 0;
				$reservasNoConsumidasOnline = 0;
				$reservasConsumidasOnline = 0;
				$reservasTotalVegetariano = 0;
				$reservasNoConsumidasVegetariano = 0;
				$reservasConsumidasVegetariano = 0;
				$turnosDisponibles = array();
				if(!empty($turnos)){//echo '<pre>';print_r($turnos);print_r($data);die;
					// agrupo por fecha
					foreach($turnos as $key => $t){
						$turno['id'] = $t->id;
						$turno['fecha'] = $t->fecha;
						$turno['cupo'] = $t->cupo;
						$cupoPorDia += intval($turno['cupo']);
						$turno['activodesde'] = $t->activodesde;
						$turno['activohasta'] = $t->activohasta;
						$turno['horario']['id'] = $t->horario->id;
						$turno['horario']['horainicio'] = $t->horario->horainicio;
						$turno['horario']['horafin'] = $t->horario->horafin;
						$turno['horario']['dia'] = $t->horario->dia;
						$turno['reservas']['total'] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado!=:estado','params'=>array('idturno'=>$t->id, 'estado'=>'E'))));
						$turno['reservas']['noconsumidas'] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado=:estado','params'=>array('idturno'=>$t->id,'estado'=>'0'))));
						$turno['reservas']['consumidas'] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND (estado=:estado1 OR estado=:estado2)','params'=>array('idturno'=>$t->id,'estado1'=>'1','estado2'=>'2'))));
						// Estadística offline
						$turno['reservas']['totaloffline'] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND token LIKE :token AND estado!=:estado','params'=>array('idturno'=>$t->id,'token'=>'%'.Yii::app()->params['codigoOffline'].'%', 'estado'=>'E'))));
						$turno['reservas']['noconsumidasoffline'] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado=:estado AND token LIKE :token ','params'=>array('idturno'=>$t->id,'estado'=>'0','token'=>'%'.Yii::app()->params['codigoOffline'].'%'))));
						$turno['reservas']['consumidasoffline'] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND (estado=:estado1 OR estado=:estado2) AND token LIKE :token ','params'=>array('idturno'=>$t->id,'estado1'=>'1','estado2'=>'2','token'=>'%'.Yii::app()->params['codigoOffline'].'%'))));

						// Estadística onnline
						$turno['reservas']['totalonline'] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND token NOT LIKE :token AND estado!=:estado','params'=>array('idturno'=>$t->id,'token'=>'%'.Yii::app()->params['codigoOffline'].'%', 'estado'=>'E'))));
						$turno['reservas']['noconsumidasonline'] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado=:estado AND token NOT LIKE :token ','params'=>array('idturno'=>$t->id,'estado'=>'0','token'=>'%'.Yii::app()->params['codigoOffline'].'%'))));
						$turno['reservas']['consumidasonline'] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND (estado=:estado1 OR estado=:estado2) AND token NOT LIKE :token ','params'=>array('idturno'=>$t->id,'estado1'=>'1','estado2'=>'2','token'=>'%'.Yii::app()->params['codigoOffline'].'%'))));

						//  Estadística vegetariano
						$turno['reservas']['totalvegetariano'] = count(self::filtroVegetariano(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado!=:estado','params'=>array('idturno'=>$t->id, 'estado'=>'E')))));
						$turno['reservas']['noconsumidasvegetariano'] = count(self::filtroVegetariano(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado=:estado','params'=>array('idturno'=>$t->id,'estado'=>'0')))));
						$turno['reservas']['consumidasvegetariano'] = count(self::filtroVegetariano(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND (estado=:estado1 OR estado=:estado2)','params'=>array('idturno'=>$t->id,'estado1'=>'1','estado2'=>'2')))));

						$reservasTotal += intval($turno['reservas']['total']);
						$reservasNoConsumidas += intval($turno['reservas']['noconsumidas']);
						$reservasConsumidas += intval($turno['reservas']['consumidas']);

						$reservasTotalOffline += intval($turno['reservas']['totaloffline']);
						$reservasNoConsumidasOffline += intval($turno['reservas']['noconsumidasoffline']);
						$reservasConsumidasOffline += intval($turno['reservas']['consumidasoffline']);

						$reservasTotalOnline += intval($turno['reservas']['totalonline']);
						$reservasNoConsumidasOnline += intval($turno['reservas']['noconsumidasonline']);
						$reservasConsumidasOnline += intval($turno['reservas']['consumidasonline']);

						$reservasTotalVegetariano += intval($turno['reservas']['totalvegetariano']);
						$reservasNoConsumidasVegetariano += intval($turno['reservas']['noconsumidasvegetariano']);
						$reservasConsumidasVegetariano += intval($turno['reservas']['consumidasvegetariano']);

						$turnosDisponibles[$t->fecha][] = $turno;
					}
		
					if(!empty($turnosDisponibles)){
						foreach($turnosDisponibles as $key => $td){
							$turnoFinal['fecha'] = $key;
							$turnoFinal['fechaString'] = self::fechaString($key);
							$turnoFinal['dia'] = $td[0]['horario']['dia'];
							$turnoFinal['diaCompleto'] = $_POST['idhorario'] == 'all' ? true : false;
							$turnoFinal['horario'] = $_POST['idhorario'] == 'all' ? 'Día completo' : $td[0]['horario']['dia'].' de '.$td[0]['horario']['horainicio'].' a '.$td[0]['horario']['horafin'];
							$turnoFinal['cupoPorDia'] = $cupoPorDia;
							$turnoFinal['reservasTotal'] = $reservasTotal;
							$turnoFinal['reservasNoConsumidas'] = $reservasNoConsumidas;
							$turnoFinal['reservasConsumidas'] = $reservasConsumidas;
							$turnoFinal['reservasTotalOffline'] = $reservasTotalOffline;
							$turnoFinal['reservasNoConsumidasOffline'] = $reservasNoConsumidasOffline;
							$turnoFinal['reservasConsumidasOffline'] = $reservasConsumidasOffline;
							$turnoFinal['reservasTotalOnline'] = $reservasTotalOnline;
							$turnoFinal['reservasNoConsumidasOnline'] = $reservasNoConsumidasOnline;
							$turnoFinal['reservasConsumidasOnline'] = $reservasConsumidasOnline;
							$turnoFinal['reservasTotalVegetariano'] = $reservasTotalVegetariano;
							$turnoFinal['reservasNoConsumidasVegetariano'] = $reservasNoConsumidasVegetariano;
							$turnoFinal['reservasConsumidasVegetariano'] = $reservasConsumidasVegetariano;
							$turnoFinal['turnos'] = $td;
							
							array_push($data, $turnoFinal);
						}
					}
				}
			}

			echo json_encode(array(
				"status"=>"ok",
				"msg"=>"Turnos encontrados.",
				"data"=>$data
			));
		}else{
			echo json_encode(array(
				'status'=>'error',
				'message'=>'No se enviaron datos POST',
				'data'=>$_POST
			));
		}
	}

	private function fechaString($fecha){
		$date = new DateTime($fecha);
		$fecha = self::diaLocal($date->format("D")).' '.explode("-",$fecha)[2].' de '.self::mesLocal(explode("-",$fecha)[1]).' '.explode("-",$fecha)[0];
		return $fecha;
	}

	private function diaLocal($d){
        $dias = array(
            'Mon'=>'Lunes',
            'Tue'=>'Martes',
            'Wed'=>'Miércoles',
            'Thu'=>'Jueves',
            'Fri'=>'Viernes',
            'Sat'=>'Sábado',
            'Sun'=>'Domingo'
        );
        return $dias[$d];
    }

	private function mesLocal($d){
        $mes = array(
            '01'=>'Enero',
            '02'=>'Febrero',
            '03'=>'Marzo',
            '04'=>'Abril',
            '05'=>'Mayo',
            '06'=>'Junio',
            '07'=>'Julio',
			'08'=>'Agosto',
			'09'=>'Septiembre',
			'10'=>'Octubre',
			'11'=>'Noviembre',
			'12'=>'Diciembre'
        );
        return $mes[$d];
    }

	private function filtroVegetariano($reservas){
		$vegetarianos = array();
		$emails = array();
		if(!empty($reservas)){
			foreach($reservas as $reserva){
				$emails[] = $reserva->email;
				
			}
			$emails = implode("','",$emails);
			$query = "SELECT * FROM talumno WHERE talumno.correo IN ('" . $emails . "') AND talumno.vegetariano='1' ";
			$vegetarianos = Yii::app()->db->createCommand($query)->queryAll();
		}
		return $vegetarianos;
	}

}