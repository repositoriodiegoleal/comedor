<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?=ucwords($this->id)?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/inicio">Inicio</a></li>
              <li class="breadcrumb-item active"><?=ucwords($this->id)?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <?php echo CHtml::link('Agregar', Yii::app()->request->baseUrl.'/turnos/agregar' , array('class'=>'btn btn-primary float-right')); ?>
              </div><!-- /.card-header -->
              <div class="card-body">
                <table id="admin-turnos" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <tr>
                      <th>Activo</th>
                      <th>Día</th>
                      <th>Cupo</th>
                      <th>Activo desde</th>
					            <th>Activo hasta</th>
                      <th>Acciones</th>
                      <th>Turnos</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($turnos)){ ?>
                      	<?php foreach($turnos as $turno){
                          $order = '';
                          switch($turno->horario->dia){
                            case 'LUN': $order='1'; break;
                            case 'MAR': $order='2'; break;
                            case 'MIE': $order='3'; break;
                            case 'JUE': $order='4'; break;
                            case 'VIE': $order='5'; break;
                            case 'SAB': $order='6'; break;
                            case 'DOM': $order='7'; break;
                          }
                          ?>
                          <tr>
                            <td>
                            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                              <input type="checkbox" class="custom-control-input chk-estado" id="chk-desktop-<?=$turno->id?>" <?=$turno->estado=='1'?'checked':'';?>>
                              <label class="custom-control-label" for="chk-desktop-<?=$turno->id?>"></label>
                            </div>
                            </td>
                            <td data-order="<?=$turno->fecha.'-'.$order.'-'.$turno->horario->horainicio?>">
                              <span><?=$turno->horario->dia?></span> <?=Utils::fechaWeb($turno->fecha)?> 
                              <p class="text-horario text-detail">de <?=$turno->horario->horainicio?> a <?=$turno->horario->horafin?></p>
                            </td>
                            <td><?=$turno->cupo?></td>
                            <td><?=Utils::fechaWeb($turno->activodesde)?></td>
                            <td><?=Utils::fechaWeb($turno->activohasta)?></td>
                            <td>
                            <a href="<?=Yii::app()->request->baseUrl.'/turnos/editar/'.$turno->id?>" class="btn btn-default"><i class="fas fa-edit"></i></a>
                            <a href="<?=Yii::app()->request->baseUrl.'/turnos/eliminar/'.$turno->id?>" class="btn btn-default btn-eliminar" data-toggle="modal" data-target="#modal-eliminar-turno"><i class="fas fa-trash-alt"></i></a>
                            </td>
                            <td>
                            <div class="box-mobile">
                              <div class="box-body">
                              <div class="box-item"><?=$turno->estado?></div>
                              <div class="box-item"><?=$turno->fecha?></div>
                              </div>
                              <div class="box-body">
                              <div class="box-item"><?=$turno->activodesde?> <?=$turno->activohasta?></div>
                              <div class="box-item">Acciones</div>
                              </div>
                            </div>
                            </td>
                        	</tr>
                        <?php }// foreach usuarios ?>
                      <?php }// if usuarios ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
		  
          </div>
          
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<?php require "modal_eliminar.php"; ?>