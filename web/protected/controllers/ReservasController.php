<?php

class ReservasController extends Controller
{
	public $layout='main_intranet';
	public function filters(){
		return array('accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules(){
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				//'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            )	
        );
	}

	public function actionIndex()
	{
		$turnos = Turnos::model()->findAll(array('condition'=>'estado!=:estado','params'=>array('estado'=>'E')));
		$reservas = array();
		foreach($turnos as $turno){
			$reservas[$turno->id] = count(Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado!=:estado','params'=>array('idturno'=>$turno->id, 'estado'=>'E'))));
		}
		$this->render('index', array('turnos'=>$turnos,'reservas'=>$reservas));
	}

	public function actionVer($id)
	{
		$model = Turnos::model()->findByPk($id);
		if($model->estado == 'E')
			$this->redirect('../../reservas');
		// Modifico el formato de las fechas para el calendario.
		$model->fecha = Utils::fechaWeb($model->fecha);
		$model->activodesde = Utils::fechaWeb($model->activodesde);
		$model->activohasta = Utils::fechaWeb($model->activohasta);
		$reservas = Reservas::model()->findAll(array('condition'=>'idturno=:idturno AND estado!=:estado','params'=>array('idturno'=>$id, 'estado'=>'E')));
		//echo '<pre>';print_r($reservas);die;
		$this->render('lista',array(
			'model'=>$model,
			'reservas'=>$reservas,
		));
	}

	public function actionAgregar($id)
	{
		$responseReserva = array();
		$modelturno = Turnos::model()->findByPk($id);
		// Modifico el formato de las fechas para el calendario.
		$modelturno->fecha = Utils::fechaWeb($modelturno->fecha);
		$modelturno->activodesde = Utils::fechaWeb($modelturno->activodesde);
		$modelturno->activohasta = Utils::fechaWeb($modelturno->activohasta);

		$permisos = Yii::app()->user->getPermisos();
		$reservaOffline = false;
		foreach($permisos as $p){
			$permiso = Permisos::model()->findByPk($p->idpermiso);
			if(!empty($permiso) && $permiso->nombre === 'reservas-offline'){
				$reservaOffline = true;
			}
		}

		$model = new Reservas;

		if(isset($_POST['Reservas']))
		{
			$emailValido = false;
			$tiporeserva = 'online';
			if($reservaOffline == true && $_POST['Reservas']['tipo']=='offline'){
				$tiporeserva = 'offline';
			}
			// VALIDACIONES
			if(!Utils::turnoDisponible($id)){
				$responseReserva = array('status'=>'error','msg'=>'No hay turnos disponibles.');
			}
			if(!Utils::reservaPorDia($_POST['Reservas']['email'],$id) && $tiporeserva == 'online'){
				$responseReserva = array('status'=>'error','msg'=>'El email ya tiene una reserva para éste día.');
			}
			if(!Utils::reservasPorSemana($_POST['Reservas']['email'],$id,Yii::app()->params['maxReservaSemana']) && $tiporeserva == 'online'){
				$responseReserva = array('status'=>'error','msg'=>'El alumno excedió el máximo de reservas por semana ('.Yii::app()->params['maxReservaSemana'].')');
			}
			if(empty($responseReserva)){
				$response = Utils::validarEmail($_POST['Reservas']['email'],$id,$tiporeserva,$_POST['Reservas']['dni']);
				//$response = array('status'=>'ok','data'=>'111,222,333');
				if($response['status'] == 'ok'){
					$emailValido = $tiporeserva=='online' || ($tiporeserva=='offline' && Yii::app()->params['guardarOffline'])?true:false;
					$token = $response['data'];
				}else{
					$emailValido = false;
					$error = array('email'=>$response['message']);
				}
				if($emailValido){
					$token = explode(",", $token);
					foreach($token as $t){
						$model = new Reservas;
						$_POST['Reservas']['token'] = $t;
						$_POST['Reservas']['idturno'] = $id;
						$_POST['Reservas']['idusuario'] = Yii::app()->user->id;
						$_POST['Reservas']['estado'] = '0';
						$model->attributes=$_POST['Reservas'];
						$response = $model->save();
					}
					
					if($response){
						//$this->redirect('../../reservas/ver/'.$id);
						$model = new Reservas; //necesario para limpiar el formulario
						$responseReserva = array('status'=>'ok','msg'=>'Reserva guardada.');
					}
				}
			}

		}
		
		$this->render('form',array(
			'modelturno'=>$modelturno,
			'model'=>$model,
			'reservaOffline'=>$reservaOffline,
			'responseReserva'=>$responseReserva,
		));
	}

	public function actionEliminar($id)
	{ 
		// Reservas::model()->findByPk($id)->delete();
		$model = Reservas::model()->findByPk($id);
		$model->estado = 'E';
		$model->update();
		$this->redirect('../../reservas/');	
	}

	private function validarEmail($email,$idturno){
		$emails = Alumnos::model()->findAll(array('condition'=>'email=:email','params'=>array('email'=>$email)));
		$reservas = Reservas::model()->findAll(array('condition'=>'email=:email AND idturno=:idturno','params'=>array('email'=>$email,'idturno'=>$idturno)));
		if(count($emails) > 0 && count($reservas) == 0){
			return array('status'=>true, 'message'=>'Email validado');
		}else if(count($emails) == 0){
			return array('status'=>false, 'message'=>'El email no corresponde a un alumno');
		}else if(count($reservas) > 0){
			return array('status'=>false, 'message'=>'El email ya tiene una reserva');
		}
	}
	
}