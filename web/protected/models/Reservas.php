<?php

/**
 * This is the model class for table "reservas".
 *
 * The followings are the available columns in table 'reservas':
 * @property integer $id
 * @property string $email
 * @property string $detalle
 * @property string $token
 * @property integer $idturno
 * @property integer $idusuario
 * @property string $estado
 *
 * The followings are the available model relations:
 * @property Turnos $idturno0
 */
class Reservas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reservas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, idturno, estado', 'required'),
			array('token','ext.ReservaValidation','campo'=>'codigo'),
			array('idturno, idusuario', 'numerical', 'integerOnly'=>true),
			array('token', 'numerical'),
			array('email', 'length', 'max'=>50),
			array('detalle', 'length', 'max'=>255),
			array('estado', 'length', 'max'=>1),
			array('token','length','max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, detalle, token, idturno, idusuario, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'turno' => array(self::BELONGS_TO, 'Turnos', 'idturno'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'detalle' => 'Detalle',
			'token' => 'Token',
			'idturno' => 'Idturno',
			'idusuario' => 'Idusuario',
			'estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('detalle',$this->detalle,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('idturno',$this->idturno);
		$criteria->compare('idusuario',$this->idusuario);
		$criteria->compare('estado',$this->estado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reservas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
