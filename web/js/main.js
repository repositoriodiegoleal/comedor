$(document).ready(function() {

    $(document).on('click','#btn-reset-password',function(e) {
        e.preventDefault();
        let form = document.querySelector('#form-reset-password')
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            form.classList.add('was-validated');
        }else{
            //console.log('formulario validado');
            resetPassword();
        }
    });

});

function resetPassword(){
    console.log('resetear password');
    let password_1 = $('#password-1').val();
    let password_2 = $('#password-2').val();
    if(password_1 != password_2){
        showMessage('error', 'ERROR', 'La confirmación de contraseña no coincide.');
        $('#modal-reset-password').modal('hide');
    }else{
        $.ajax({
            url: urlSite+'/usuarios/resetpassword',
            data: {
                password_1:password_1,
                password_2:password_2
            },
            type: 'post',
            chache: false,
            beforeSend: function() {
                //console.log('antes de enviar la solicitud');
                openLoading();
            },
        })
        .done(function (response) {
            console.log(response);
            var obj = JSON.parse(response);
            if (obj.status === 'ok') {
                showMessage('success', 'OK', 'Se ha cambiado la contraseña correctamente.');
            }
        })
        .fail(function(jqXHR, textStatus, errorThrown ) {
            console.log('Error update-estado:',textStatus,errorThrown);
            showMessage('error', 'ERROR', 'Se produjo un error inesperado. Contacte con el administrador.');
        })
        .always(function() {
            //console.log('despues de enviar la solicitud');
            closeLoading();
            $('#modal-reset-password').modal('hide');
        });
    }
}

function openLoading(){
    let loading = '<div class="loading"><i class="fas fa-3x fa-sync-alt fa-spin"></i></div>';
    $('body').prepend(loading);
}

function closeLoading(){
    setTimeout(function(){
        $('body .loading').remove();
    }, 300);// retardo de 1 seg.
}

function showMessage(type, title, msg){
    let iconToast = '';
    let classToast = '';
    switch(type){
        case 'success': classToast = 'bg-success'; iconToast = 'far fa-check-circle'; break;
        case 'error': classToast = 'bg-danger'; iconToast = 'fas fa-times-circle'; break;
        case 'warning': classToast = 'bg-warning'; iconToast = 'fas fa-exclamation-triangle'; break;
    }
    $(document).Toasts('create', {
        body: msg,
        title: title,
        icon: iconToast,
        class: classToast,
        autohide: true,
        delay: 5000,
      })
}

function activeMenuItem(id){
    let items = document.querySelectorAll('.nav-link');
    if(items.length > 0){
        items.forEach(function(item, key){
            item.classList.remove('active');
        });
    }
    document.querySelector('.nav-link-'+id).classList.add('active');
}