<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is displayed in the header section
	'title'=>'My Yii Blog',
	// this is used in error pages
	'adminEmail'=>'webmaster@example.com',
	// number of posts displayed per page
	'postsPerPage'=>10,
	// maximum number of comments that can be displayed in recent comments portlet
	'recentCommentCount'=>10,
	// maximum number of tags that can be displayed in tag cloud portlet
	'tagCloudCount'=>20,
	// whether post comments need to be approved before published
	'commentNeedApproval'=>true,
	// the copyright information displayed in the footer section
	'copyrightInfo'=>'Copyright &copy; 2009 by My Company.',
	// Al dar de alta un usuario se puede utilizar este password por defecto.
	'passwordDefault'=>'123456',
	'tokenDefault'=>'654321',
	'maxReservaSemana'=>3,
	'direccionSbu'=>'Av. Bolivia n° 190 Bº Los Huaicos - S. S. de Jujuy',
	'telefonoSbu'=>'3884397567',
	'emailSbu'=>'comedor@unju.edu.ar',
	'verTurnoNoDisponible'=>true,
	'guardarOffline'=>true,
	'asuntoCorreo'=>'Reserva de Turno-Comedor Universitario- UNJu',
	'dirGuardarArchTemp'=>'./protected/runtime/pdf',
	'dirGuardarArchTempTxt'=>'./protected/runtime/Txt',
	'cantidadDiasHabiles'=>1,
	'urlComedor'=>'http://localhost/comedor/web',
	'tiempoMaxCancelacion'=>1,//1 signica la cantidad de días
	'mensajeReservaEliminada'=>'La reserva se elimino.<br>Si no puede seleccionar otro turno comuniquese mediante los contatos que figuran arriba.</br>',
	'mensajeReservaNoEliminada'=>'No se pudo eliminar la reserva, se termino el plazo de eliminar.',
	'codigoOffline'=>'09999',
);