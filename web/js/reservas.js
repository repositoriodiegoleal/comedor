$(document).ready(function() {
    console.log('init reservas');
    
    activeMenuItem('reservas');

    initTablaReservas();
    initTablaReservaDetalles();

    $(document).on('click','.btn-eliminar',function(e) {
        e.preventDefault();
        $("#btn-continuar-reserva").attr("href", $(this).attr('href'));
    });

    $(document).on('change','#tipo',function() {
        console.log($(this).val());
        if($(this).val() == 'offline'){
            $('.dni-group').show();
        }else{
            $('.dni-group').hide();
        }
    });
});

function initTablaReservas(){
    window.tablaHorarios = $('#admin-reservas').DataTable({
        //"dom": '<"top"i>rt<"bottom"flp><"clear">',
        //"dom": '<"myfilter"f><"mylength"l>t<"myinfo"i><"mypaginate"p><"clear">',
        //"dom": '<"myfilter"f>t<"myinfo"i><"mypaginate"p><"clear">',
        //"dom": 't<"myinfo"i><"mypaginate"p><"clear">',
        "responsive": { details: false},
        "order": [[ 1, "asc" ]],
        "columnDefs":[{
            "targets": [3],
            "orderable":false
        }],
        "language": {
            "processing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron resultados",
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "0 registros encontrados",
            "infoFiltered": "(filtrado de un total de _MAX_ registro/s)",
            "search": "Buscar:",
            "infoThousands": ",",
            "loadingRecords": "Cargando...",
            "paginate": {
                "previous": "Siguiente",
                "next": "Anterior",
                "first": "Primera",
                "last": "Ultima"
            },
            "info": "_TOTAL_ registro/s encontrado/s"
        },
        "columns": [
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 1, className: "mobile-p mobile-l tablet-p"},
        ],
        "initComplete": function () {
            
        }
    });
}

function initTablaReservaDetalles(){
    window.tablaHorarios = $('#admin-reservas-detalle').DataTable({
        //"dom": '<"top"i>rt<"bottom"flp><"clear">',
        //"dom": '<"myfilter"f><"mylength"l>t<"myinfo"i><"mypaginate"p><"clear">',
        //"dom": '<"myfilter"f>t<"myinfo"i><"mypaginate"p><"clear">',
        //"dom": 't<"myinfo"i><"mypaginate"p><"clear">',
        "responsive": { details: false},
        "order": [[ 0, "asc" ]],
        "columnDefs":[{
            "targets": [2],
            "orderable":false
        }],
        "language": {
            "processing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron resultados",
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "0 registros encontrados",
            "infoFiltered": "(filtrado de un total de _MAX_ registro/s)",
            "search": "Buscar:",
            "infoThousands": ",",
            "loadingRecords": "Cargando...",
            "paginate": {
                "previous": "Siguiente",
                "next": "Anterior",
                "first": "Primera",
                "last": "Ultima"
            },
            "info": "_TOTAL_ registro/s encontrado/s"
        },
        "columns": [
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 2, className: "tablet-l desktop"},
            { responsivePriority: 1, className: "mobile-p mobile-l tablet-p"},
        ],
        "initComplete": function () {
            
        }
    });
}
