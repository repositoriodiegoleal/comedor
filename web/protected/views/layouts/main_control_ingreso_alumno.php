<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Comedor Universitario - Control de Ingreso</title>
    <link rel="icon" type="image/png" href="<?=Yii::app()->request->baseUrl?>/img/icon.png">
      <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->baseUrl?>/themes/adminlte/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" >
    <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/css/style.css" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl?>/css/landing.css" />
    <link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl?>/css/controlingreso.css?t=<?=time()?>" />
    <script> var urlSite = '<?=Yii::app()->request->baseUrl?>';</script>
</head>
<body>
    <!-- ======= Top Bar ======= -->
    <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
        <div class="container d-flex">
            <div class="contact-info mr-auto">
                <i class="icofont-google-map"></i> <?=Yii::app()->params['direccionSbu']?>
            </div>
            <div class="contact-info ml-auto">
                <i class="icofont-envelope"></i> <a href="mailto:<?=Yii::app()->params['emailSbu']?>"><?=Yii::app()->params['emailSbu']?></a>
                <i class="icofont-phone"></i> <?=Yii::app()->params['telefonoSbu']?>
            </div>
        </div>
    </div>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">
            <h1 class="logo mr-auto"><a href="<?=Yii::app()->request->baseUrl?>">Comedor Universitario</a></h1>
            <?php if($this->id=='controlingresoalumno'){ ?>
                <a href="<?=Yii::app()->request->baseUrl?>/logout" class="appointment-btn scrollto">Cerrar Sesión</a>
            <?php } ?>
        </div>
    </header><!-- End Header -->


    <?php echo $content; ?>

    <!-- ======= Footer ======= -->
    <footer id="footer">

    <div class="container d-md-flex align-items-center py-4">

    <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
        &copy; Secretaria de <strong><span>Bienestar Universitario</span></strong> 2022
        </div>
    </div>
    <div class="text-center text-md-right pt-3 pt-md-0">
        <a href="#" class=""><img class="logo_comedor" src="<?=Yii::app()->request->baseUrl.'/img/logo_comedor.png'?>"></img></a>
        <a href="#" class=""><img class="logo_sbu" src="<?=Yii::app()->request->baseUrl.'/img/logo_sbu.png'?>"></img></a>
        <a href="#" class=""><img class="logo_unju" src="<?=Yii::app()->request->baseUrl.'/img/logo_unju.png'?>"></img></a>
    </div>
    </div>
    </footer><!-- End Footer -->
    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
    

    <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/venobox/venobox.min.js"></script>
    <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/counterup/counterup.min.js"></script>
    <!-- Template Main JS File -->
    <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/js/main.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/controlingreso.js?t=<?=time()?>"></script>
</body>
</html>