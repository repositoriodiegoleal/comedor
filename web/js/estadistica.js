$(document).ready(function() {
    console.log('init estadistica');
    
    activeMenuItem('estadistica');
    initTablaTurnos()
    initCalendarios();

    $(document).on('click','#btn-buscar',function(e) {
        e.preventDefault();
        actualizarEstadistica();
    });

});

function initCalendarios(){
    $('.calendar').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Aceptar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizar",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        // "startDate": "2016-01-01",
        // "endDate": "2016-01-07",
        // "minDate": moment(),
        "opens": "center",
        // "maxSpan": {
        //     "days": 7
        // },
        "autoApply": true
    });

    // $('.single-calendar').on('apply.daterangepicker', function(ev, picker) {
    //     $(this).val(picker.startDate.format('DD/MM/YYYY'));
    // });

    // $('.single-calendar').trigger('apply.daterangepicker');
}

function actualizarEstadistica(){
    fechaCalendario = $('#Estadistica_fecha').val();
    idHorario = $('#Estadistica_horario').val();
    $.ajax({
        url: urlSite+'/estadistica/buscar',
        data: {
            fecha:fechaCalendario,
            idhorario:idHorario,
        },
        type: 'post',
        chache: false,
        beforeSend: function() {
            
        },
    })
    .done(function (response) {
        //console.log(response);
        var obj = JSON.parse(response);
        if($.fn.dataTable.isDataTable("#turnos")) $('#turnos').DataTable().destroy();
        $('#turnos tbody tr').remove();
        if (obj.status === 'ok') {
            let items = obj.data;
            if(items.length > 0){
                items.forEach(function(item, key){
                    if($('#reservaOffline').val()=='1'){
                        $('#turnos tbody').append(htmlRowTurnoOffline(item));
                    }else{
                        $('#turnos tbody').append(htmlRowTurnoOnline(item));
                    }
                });
                
            }
        }
        initTablaTurnos();
    })
    .fail(function(jqXHR, textStatus, errorThrown ) {
        console.log('error guardar ficha',textStatus,errorThrown);
    })
    .always(function() {
        
    });
}

function initTablaTurnos(){
    window.tablaHorarios = $('#turnos').DataTable({
        "pageLength": 50,
        "searching": false,
        "lengthChange": false,
        "info": false,
        "responsive": { details: false},
        "order": [[ 0, "asc" ]],
        "language": {
            "processing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ turnos",
            "zeroRecords": "No se encontraron resultados",
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "0 turnos encontrados",
            "infoFiltered": "(filtrado de un total de _MAX_ turno/s)",
            "search": "Buscar:",
            "infoThousands": ",",
            "loadingRecords": "Cargando...",
            "paginate": {
                "previous": "Siguiente",
                "next": "Anterior",
                "first": "Primera",
                "last": "Ultima"
            },
            "info": "_TOTAL_ turno/s encontrado/s"
        },
        "initComplete": function () {
            
        }
    });
}

function htmlRowTurnoOnline(item){
    return `
    <tr class="row-turno-desktop">
        <td class="turno-dia" data-order="${item.fecha}-${ordenDia(item.horario.dia)}-${item.horario.horainicio}">${item.dia} ${item.fecha.split('-')[2]}/${item.fecha.split('-')[1]}/${item.fecha.split('-')[0]} </td>
        <td class="turno-dia" > <p class="text-horario text-detail">${item.horario}</p> </td>
        <td class="turno-dia" > ${item.cupoPorDia} </td>
        <td class="turno-dia" > <p class="reserva-total">${item.reservasTotal}</p>
            <p class="text-horario text-detail text-vegetariano">Vegetarianos: ${item.reservasTotalVegetariano}</p>
            <p class="text-horario text-detail text-vegetariano">No vegetarianos: ${item.reservasTotal - item.reservasTotalVegetariano}</p> 
        </td>
        <td class="turno-dia" > <p class="reserva-consumida">${item.reservasConsumidas}</p>
            <p class="text-horario text-detail text-vegetariano">Vegetarianos: ${item.reservasConsumidasVegetariano}</p>
            <p class="text-horario text-detail text-vegetariano">No vegetarianos: ${item.reservasConsumidas - item.reservasConsumidasVegetariano}</p>
        </td>
        <td class="turno-dia" > <p class="reserva-noconsumida">${item.reservasNoConsumidas}</p>
            <p class="text-horario text-detail text-vegetariano">Vegetarianos: ${item.reservasNoConsumidasVegetariano}</p>
            <p class="text-horario text-detail text-vegetariano">No vegetarianos: ${item.reservasNoConsumidas - item.reservasNoConsumidasVegetariano}</p>
        </td>
    </tr>
    `;
}

function htmlRowTurnoOffline(item){
    return `
    <tr class="row-turno-desktop">
        <td class="turno-dia" data-order="${item.fecha}-${ordenDia(item.horario.dia)}-${item.horario.horainicio}">${item.dia} ${item.fecha.split('-')[2]}/${item.fecha.split('-')[1]}/${item.fecha.split('-')[0]} </td>
        <td class="turno-dia" > <p class="text-horario text-detail">${item.horario}</p> </td>
        <td class="turno-dia" > ${item.cupoPorDia} </td>
        <td class="turno-dia" > <p class="reserva-total">${item.reservasTotal}</p>
            <p class="text-horario text-detail text-vegetariano">Vegetarianos: ${item.reservasTotalVegetariano}</p>
            <p class="text-horario text-detail text-vegetariano">No vegetarianos: ${item.reservasTotal - item.reservasTotalVegetariano}</p> 
            <p class="text-horario text-detail">Online: ${item.reservasTotalOnline}</p>
            <p class="text-horario text-detail">Offline: ${item.reservasTotalOffline}</p>
        </td>
        <td class="turno-dia" > <p class="reserva-consumida">${item.reservasConsumidas}</p>
            <p class="text-horario text-detail text-vegetariano">Vegetarianos: ${item.reservasConsumidasVegetariano}</p>
            <p class="text-horario text-detail text-vegetariano">No vegetarianos: ${item.reservasConsumidas - item.reservasConsumidasVegetariano}</p>
            <p class="text-horario text-detail">Online: ${item.reservasConsumidasOnline}</p>
            <p class="text-horario text-detail">Offline: ${item.reservasConsumidasOffline}</p>
        </td>
        <td class="turno-dia" > <p class="reserva-noconsumida">${item.reservasNoConsumidas}</p>
            <p class="text-horario text-detail text-vegetariano">Vegetarianos: ${item.reservasNoConsumidasVegetariano}</p>
            <p class="text-horario text-detail text-vegetariano">No vegetarianos: ${item.reservasNoConsumidas - item.reservasNoConsumidasVegetariano}</p>
            <p class="text-horario text-detail">Online: ${item.reservasNoConsumidasOnline}</p>
            <p class="text-horario text-detail">Offline: ${item.reservasNoConsumidasOffline}</p>
        </td>
    </tr>
    `;
}

function ordenDia(dia){
    switch(dia){
        case 'LUN': dia = '1'; break;
        case 'MAR': dia = '2'; break;
        case 'MIE': dia = '3'; break;
        case 'JUE': dia = '4'; break;
        case 'VIE': dia = '5'; break;
        case 'SAB': dia = '6'; break;
        case 'DOM': dia = '7'; break;
    }
    return dia;
}