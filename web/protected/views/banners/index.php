<!-- ======= Testimonials Section ======= -->
<section id="banners_home" class="banners_home">
	<div class="">

		<div class="owl-carousel owl-theme">
			<div class="item">
				<div class="img-banner" style="background-image: url('<?=Yii::app()->request->baseUrl.'/img/banners/b1.jpeg'?>'); margin-bottom: 30px;">
					<div class="img-mask">&nbsp;</div>
					<div class="text-center white resoluciones">
						<!-- <h2>COMEDOR UNIVERSITARIO</h2>
						<hr>
						<h3>Universidad Nacional de Jujuy</h3> -->
					</div>
				</div>
			</div>
			<div class="item">
				<div class="img-banner" style="background-image: url('<?=Yii::app()->request->baseUrl.'/img/banners/b3.jpeg'?>'); margin-bottom: 30px;">
					<div class="img-mask">&nbsp;</div>
					<div class="text-center white resoluciones">
						<!-- <h2>COMEDOR UNIVERSITARIO</h2>
						<hr>
						<h3>Universidad Nacional de Jujuy</h3> -->
					</div>
				</div>
			</div>
			<div class="item">
				<div class="img-banner" style="background-image: url('<?=Yii::app()->request->baseUrl.'/img/banners/b4.jpeg'?>'); margin-bottom: 30px;">
					<div class="img-mask">&nbsp;</div>
					<div class="text-center white resoluciones">
						<!-- <h2>COMEDOR UNIVERSITARIO</h2>
						<hr>
						<h3>Universidad Nacional de Jujuy</h3> -->
					</div>
				</div>
			</div>
		</div>

	</div>
</section><!-- End Testimonials Section -->