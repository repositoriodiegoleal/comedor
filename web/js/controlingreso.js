$(document).ready(function() {
    console.log('init control ingreso');
    $(document).on('keyup','#ValidarIngreso_codigo',function() {
        console.log( $(this).val());
        let codigo = $(this).val();
        if(codigo.length==21){
            console.log('actualizar tabla');
            actualizarTabla(codigo);
        }
    });
    $(document).on('keyup','#ValidarIngreso_dni',function() {
        console.log( $(this).val());
        let dni = $(this).val();
        if(dni.length==8 || dni.length==9){
            console.log('actualizar tabla');
            actualizarTabla(dni);
        }
    });
});

function actualizarTabla(codigo){
    $.ajax({
        url: urlSite+'/controlingresoalumno/buscar',
        data: {
            codigo:codigo
        },
        type: 'post',
        chache: false,
        beforeSend: function() {
            
        },
    })
    .done(function (response) {
        $('#bodycargadatos tr').remove();
        console.log(response);
        var obj = JSON.parse(response);
        if (obj.status === 'ok') {
            let horarios = obj.data;
            console.log(horarios);
            if(horarios.length > 0){
                let total = 0;
                $('#total-ingresos').val(total);
                horarios.forEach(function(item, key){
                    $('#bodycargadatos').append(htmlRowHorario(item));
                    total += parseInt(item.cantidad);
                });
                $('#total-ingresos').val(total);
            }
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown ) {
        console.log('Error update-estado:',textStatus,errorThrown);
    })
    .always(function() {
        
    });
}
function htmlRowHorario(item){
    return `
    <tr>
        <td> ${item.horainiciofin} </td>
        <td class='cantidad-ingreso'> ${item.cantidad} </td>
    </tr>
    `;
}