<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?=ucwords($this->id)?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/inicio">Inicio</a></li>
              <li class="breadcrumb-item active"><?=ucwords($this->id)?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <?php echo CHtml::link('Agregar', Yii::app()->request->baseUrl.'/usuarios/agregar' , array('class'=>'btn btn-primary float-right')); ?>
              </div><!-- /.card-header -->
              <div class="card-body">
                <table id="admin-usuarios" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <tr>
                      <th>Activo</th>
                      <th>Usuario</th>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Acciones</th>
                      <th>Usuarios</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($usuarios)){ ?>
                      <?php foreach($usuarios as $usuario){ ?>
                        <tr>
                            <td>
                              <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                <input type="checkbox" class="custom-control-input chk-estado" id="chk-desktop-<?=$usuario->id?>" <?=$usuario->estado=='1'?'checked':'';?>>
                                <label class="custom-control-label" for="chk-desktop-<?=$usuario->id?>"></label>
                              </div>
                            </td>
                            <td><?=$usuario->username?></td>
                            <td><?=$usuario->nombre?></td>
                            <td><?=$usuario->apellido?></td>
                            <td>
                              <a href="<?=Yii::app()->request->baseUrl.'/usuarios/editar/'.$usuario->id?>" class="btn btn-default"><i class="fas fa-edit"></i></a>
                              <a href="<?=Yii::app()->request->baseUrl.'/usuarios/eliminar/'.$usuario->id?>" class="btn btn-default btn-eliminar" data-toggle="modal" data-target="#modal-eliminar-usuario"><i class="fas fa-trash-alt"></i></a>
                            </td>
                            <td>
                              <div class="box-mobile">
                                <div class="box-body">
                                  <div class="box-item"><?=$usuario->estado?></div>
                                  <div class="box-item"><?=$usuario->username?></div>
                                </div>
                                <div class="box-body">
                                  <div class="box-item"><?=$usuario->nombre?> <?=$usuario->apellido?></div>
                                  <div class="box-item">Acciones</div>
                                </div>
                              </div>
                            </td>
                        </tr>
                        <?php }// foreach usuarios ?>
                      <?php }// if usuarios ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
		  
          </div>
          
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<?php require "modal_eliminar.php"; ?>