<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?=Yii::app()->controller->action->id?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/inicio">Inicio</a></li>
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/turnos">Turnos</a></li>
              <li class="breadcrumb-item active">Editar</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Turno</h3>
                        </div><!-- /.card-header -->
                        <?php 
                            $form=$this->beginWidget('CActiveForm', array(
                                //'action'=>Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/login',
                                //'method'=>'post',
                                'id'=>'horario-form',
                                'enableAjaxValidation'=>false,
                                //'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                            )); 
                        ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success float-right">
                                                <?php //echo $form->checkBox($model,'estado',array('class'=>'custom-control-input', 'id'=>'chk-'.$model->id,'checked'=>'checked')); ?>
                                                <input type="checkbox" class="custom-control-input" name="Turnos[estado]" id="chk-<?=$model->id?>" <?=$model->estado=='1'?'checked':'';?>>
                                                <label class="custom-control-label" for="chk-<?=$model->id?>">Activo</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($model,'fecha'); ?>
                                            <?php echo $form->textField($model,'fecha',array('class'=>'form-control single-calendar','disabled'=>'disabled')); ?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($model,'cupo'); ?>
                                            <?php echo $form->numberField($model,'cupo',array('class'=>'form-control','min'=>'0')); ?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($model,'activodesde'); ?>
                                            <?php echo $form->textField($model,'activodesde',array('class'=>'form-control single-calendar')); ?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($model,'activohasta'); ?>
                                            <?php echo $form->textField($model,'activohasta',array('class'=>'form-control single-calendar')); ?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="Turnos_permisos" class="required">Horario</label>
                                            <?php $listaPermisos = CHtml::listData(Horarios::model()->findAll(array('condition'=>'estado=:estado','params'=>array('estado'=>'1'),'order'=>'dia ASC, horainicio ASC')), 'id', function($horario){ return $horario->dia." de ".$horario->horainicio." a ".$horario->horafin; }); ?>
                                            <?php echo $form->dropDownList($model,'idhorario',$listaPermisos,array('class'=>'form-control select2','disabled'=>'disabled')); ?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary btn-guardar')); ?>
                                <?php echo CHtml::link('Cancelar', Yii::app()->request->baseUrl.'/turnos' , array('class'=>'btn btn-default')); ?>
                            </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- /.card -->
                </div>
            </div> <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section><!-- /.content -->
</div>
<script>
    var action = '<?=Yii::app()->controller->action->id?>';
</script>