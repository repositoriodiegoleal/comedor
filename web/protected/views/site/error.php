<!-- Main content -->
<section class="content">
	<div class="error-page">
		<div class="error-content">
			<h2 class="headline text-warning"><?=$code?></h2>
			<h3><i class="fas fa-exclamation-triangle text-warning"></i><?=$messageWeb?></h3>
		</div>
		<p class="errorWeb"><?php echo CHtml::encode($message); ?></p>
	</div>
	<!-- /.error-page -->
</section>
<!-- /.content -->