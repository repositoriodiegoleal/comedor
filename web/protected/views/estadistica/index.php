<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?=ucwords($this->id)?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/inicio">Inicio</a></li>
              <li class="breadcrumb-item active"><?=ucwords($this->id)?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
				
				<div class="card-header">
					<!-- <button class="btn btn-success float-right">Exportar a Excel</button> -->
				</div>

				<div class="card-body">
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label for="Estadistica_fecha" class="required">Fecha</label>
								<input type="text" name="Estadistica[fecha]" class="form-control calendar" id="Estadistica_fecha" autocomplete="off">
								<div class="invalid-feedback">
									Complete el campo.
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="Estadistica_horario" class="required">Horario</label>
								<select class="form-control select2" name="Estadistica[horario]" id="Estadistica_horario">
									<option value="all">Día completo</option>
									<?php $listaPermisos = CHtml::listData(Horarios::model()->findAll(array('condition'=>'estado=:estado','params'=>array('estado'=>'1'),'order'=>'dia ASC, horainicio ASC')), 'id', function($horario){ return $horario->dia." de ".$horario->horainicio." a ".$horario->horafin; }); ?>
									<?php foreach($listaPermisos as $key => $permiso){?>
										<option value="<?=$key?>"><?=$permiso?></option>
									<?php } ?>
								</select>
								
								<div class="invalid-feedback">
									Complete el campo.
								</div>
							</div>
						</div>
						
						<div class="col-sm-5 d-flex align-items-end">
							<div class="form-group">
								<button class="btn btn-primary" id="btn-buscar">Consultar</button>
							</div>
						</div>
					</div>
					<input type="hidden" id="reservaOffline" value="<?=$reservaOffline?>" />
					<table id="turnos" class="table table-bordered table-striped" style="width:100%">
					<thead>
						<tr>
						<th>Fecha</th>
						<th>Horario</th>
						<th>Cupo</th>
						<th>Reservas realizadas</th>
						<th>Reservas consumidas</th>
						<th>Reservas sin consumir</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
					</table>
				</div>
				<!-- /.card-body -->
            </div>
		  
          </div>
          
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
