<?php

class SiteController extends Controller
{
	public $layout='main_site';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionIndex(){
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout = 'main_page';
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest){
				echo $error['message'];
			} else {
				$error['messageWeb'] = '¡UPS! Hemos tenido un problema.';
				if(!empty($error['code'])){
					switch($error['code']){
						case '404': $error['messageWeb'] = '¡UPS! Página no encontrada.'; break;
						case '500': $error['messageWeb'] = '¡UPS! Algo salió mal.'; break;						
					}
				}
				$this->render('error', $error);
			}
	        	
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionTurnosDisponibles()
	{
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$hoy = date('Y-m-d');
		if(isset($_POST)){
			$fecha = Utils::fechaBD($_POST['fecha']);
			$turnos = Turnos::model()->findAll(array('condition'=>'fecha>=:fecha AND activodesde<=:hoy AND activohasta>=:hoy AND estado!=:estado1 AND estado!=:estado2','params'=>array('fecha'=>$fecha,'hoy'=>$hoy,'estado1'=>'0','estado2'=>'E')));
			$turnosDisponibles = array();
			$data = array();
			if(!empty($turnos)){
				// agrupo por fecha
				foreach($turnos as $key => $t){
					$turno['id'] = $t->id;
					$turno['fecha'] = $t->fecha;
					$turno['cupo'] = $t->cupo;
					$turno['disponible'] = Utils::turnoDisponible($t->id);
					$turno['activodesde'] = $t->activodesde;
					$turno['activohasta'] = $t->activohasta;
					$turno['horario']['id'] = $t->horario->id;
					$turno['horario']['horainicio'] = $t->horario->horainicio;
					$turno['horario']['horafin'] = $t->horario->horafin;
					$turno['horario']['dia'] = $t->horario->dia;
					$turnosDisponibles[$t->fecha][] = $turno;
				}
	
				// se arma response para el front
				$data = array();
				if(!empty($turnosDisponibles)){
					foreach($turnosDisponibles as $key => $td){
						$turnoFinal['fecha'] = $key;
						$turnoFinal['fechaString'] = self::fechaString($key);
						$turnoFinal['dia'] = $td[0]['horario']['dia'];
						$turnoFinal['turnos'] = $td;
						array_push($data, $turnoFinal);
					}
				}
			}
	
			echo json_encode(array(
				"status"=>"ok",
				"msg"=>"Turnos disponibles.",
				"data"=>$data
			));
		}else{
			echo json_encode(array(
				'status'=>'error',
				'message'=>'No se enviaron datos POST',
				'data'=>$_POST
			));
		}
	}

	/** deprecado? */
	private function turnoDisponible($idTurno, $cupoTurno){
		$reservas = Reservas::model()->findAll(array('condition'=>'idTurno=:idTurno','params'=>array('idTurno'=>$idTurno)));
		return count($reservas) < intval($cupoTurno);
	}

	private function diaString($dia){
		switch($dia){
			case 'LUN': $dia = 'Lunes';
				break;
			case 'MAR': $dia = 'Martes';
				break;
			case 'MIE': $dia = 'Miércoles';
				break;
			case 'JUE': $dia = 'Jueves';
				break;
			case 'VIE': $dia = 'Viernes';
				break;
			case 'SAB': $dia = 'Sábado';
				break;
			case 'DOM': $dia = 'Domingo';
				break;
		}
		return $dia;
	}

	private function fechaString($fecha){
		$date = new DateTime($fecha);
		$fecha = self::diaLocal($date->format("D")).' '.explode("-",$fecha)[2].' de '.self::mesLocal(explode("-",$fecha)[1]).' '.explode("-",$fecha)[0];
		return $fecha;
	}

	private function diaLocal($d){
        $dias = array(
            'Mon'=>'Lunes',
            'Tue'=>'Martes',
            'Wed'=>'Miércoles',
            'Thu'=>'Jueves',
            'Fri'=>'Viernes',
            'Sat'=>'Sábado',
            'Sun'=>'Domingo'
        );
        return $dias[$d];
    }

	private function mesLocal($d){
        $mes = array(
            '01'=>'Enero',
            '02'=>'Febrero',
            '03'=>'Marzo',
            '04'=>'Abril',
            '05'=>'Mayo',
            '06'=>'Junio',
            '07'=>'Julio',
			'08'=>'Agosto',
			'09'=>'Septiembre',
			'10'=>'Octubre',
			'11'=>'Noviembre',
			'12'=>'Diciembre'
        );
        return $mes[$d];
    }

	public function actionReservar()
	{
		$model = new Reservas;
		if(isset($_POST))
		{
			if(Utils::turnoDisponible($_POST['idturno'])){
				if(!Utils::reservaPorDia($_POST['email'],$_POST['idturno'])){
					echo json_encode(array(
						'status'=>'error',
						'message'=>'El email ya tiene una reserva',
						'data'=>$_POST
					));
					die;
				}
				if(!Utils::reservasPorSemana($_POST['email'],$_POST['idturno'],Yii::app()->params['maxReservaSemana'])){
					echo json_encode(array(
						'status'=>'error',
						'message'=>'Excedió el máximo de reservas',
						'data'=>$_POST
					));
					die;
				}

				$emailValido = false;
				$response = Utils::validarEmail($_POST['email'],$_POST['idturno'],'online',null);
				// $response = array('status'=>'ok','data'=>'123');
				if($response['status'] == 'ok'){
					$emailValido = true;
					$token = $response['data'];
				}else{
					$emailValido = false;
				}
				if($emailValido){
					$_POST['Reserva']['email'] = $_POST['email'];
					$_POST['Reserva']['detalle'] = 'Reserva alumno';
					$_POST['Reserva']['token'] = $token;
					$_POST['Reserva']['idturno'] = $_POST['idturno'];
					$_POST['Reserva']['idusuario'] = null;
					$_POST['Reserva']['estado'] = '0';
					$model->attributes=$_POST['Reserva'];
					if($model->save()){
						echo json_encode(array(
							'status'=>'ok',
							'message'=>'Reserva guardada',
							'data'=>$_POST['Reserva']
						));
					}
				}else{
					echo json_encode($response);
				}
			}else{
				echo json_encode(array(
					'status'=>'error',
					'message'=>'No hay turnos disponibles',
					'data'=>$_POST
				));
			}
			
		}else{
			echo json_encode(array(
				'status'=>'error',
				'message'=>'No se enviaron datos POST',
				'data'=>$_POST
			));
		}
		
	}
}
