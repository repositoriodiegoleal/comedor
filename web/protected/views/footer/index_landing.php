<!-- ======= Footer ======= -->
<footer id="footer">

<div class="container d-md-flex align-items-center py-4">

  <div class="mr-md-auto text-center text-md-left">
    <div class="copyright">
      &copy; Secretaria de <strong><span>Bienestar Universitario</span></strong> 2022
    </div>
  </div>
  <div class="text-center text-md-right pt-3 pt-md-0">
    <a href="#" class=""><img class="logo_comedor" src="<?=Yii::app()->request->baseUrl.'/img/logo_comedor.png'?>"></img></a>
    <a href="#" class=""><img class="logo_sbu" src="<?=Yii::app()->request->baseUrl.'/img/logo_sbu.png'?>"></img></a>
    <a href="#" class=""><img class="logo_unju" src="<?=Yii::app()->request->baseUrl.'/img/logo_unju.png'?>"></img></a>
  </div>
</div>
</footer><!-- End Footer -->
<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>