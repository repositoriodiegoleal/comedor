<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?=Yii::app()->controller->action->id?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/inicio">Inicio</a></li>
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/turnos">Turnos</a></li>
              <li class="breadcrumb-item active">Editar</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Turno</h3>
                        </div><!-- /.card-header -->
                        <form action="<?=Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/agregar'?>" method="post">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success float-right">
                                                <input type="checkbox" class="custom-control-input" name="Turnos[estado]" id="chk-estado">
                                                <label class="custom-control-label" for="chk-estado">Activo</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="Turnos_fecha" class="required">Fecha *</label>
                                            <input type="text" name="Turnos[fecha]" class="form-control calendar" id="Turnos_fecha" autocomplete="off">
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="Turnos_cupo" class="required">Cupo *</label>
                                            <input type="number" name="Turnos[cupo]" class="form-control" id="Turnos_cupo" min="0" value="0">
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="Turnos_desde" class="required">Activo desde *</label>
                                            <input type="text" name="Turnos[activodesde]" class="form-control single-calendar" id="Turnos_desde" autocomplete="off">
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="Turnos_hasta" class="required">Activo hasta *</label>
                                            <input type="text" name="Turnos[activohasta]" class="form-control single-calendar" id="Turnos_hasta" autocomplete="off">
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="Turnos_horario" class="required">Horario *</label>
                                            <?php $listaHorarios = CHtml::listData(Horarios::model()->findAll(array('condition'=>'estado=:estado','params'=>array('estado'=>'1'),'order'=>'dia ASC, horainicio ASC')), 'id', function($horario){ return $horario->dia." de ".$horario->horainicio." a ".$horario->horafin; }); ?>
                                            <?php //echo '<pre>';print_r($listaHorarios);die;?>
                                            <select class="form-control select2" name="Turnos[horario]" id="Turnos_horario">
                                                <option value="all">Todos los horarios</option>
                                                <?php foreach($listaHorarios as $key => $horario){ ?>
                                                    <option value="<?=$key?>"><?=$horario?></option>
                                                <?php } ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary btn-guardar')); ?>
                                <?php echo CHtml::link('Cancelar', Yii::app()->request->baseUrl.'/turnos' , array('class'=>'btn btn-default')); ?>
                            </div>
                        </form>
                    </div><!-- /.card -->
                </div>
            </div> <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section><!-- /.content -->
</div>
<script>
    var action = '<?=Yii::app()->controller->action->id?>';
</script>