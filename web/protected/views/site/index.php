
<main id="main">
    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Selecciona tu turno</h2>
          <p>Bienvenido al <strong>SISTEMA DE TURNOS DEL COMEDOR UNIVERSITARIO</strong> dependiente de la <strong>Secretaria de Bienestar Universitario de la Universidad Nacional de Jujuy</strong> mediante el mismo podrás realizar tus reservas en los diferentes turnos habilitados.</p>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6 mt-2 mb-5 mb-md-2">
                <form class="php-email-form">
                    <div class="form-row">
                        <label for="datetimepicker" class="col-md-2 col-form-label text-md-right font-weight-bold">Desde:</label>
                        <div class="col-md-6 form-group">
                            <div class="input-group date" id="datetimepicker">
                                <!-- <input type="text" readonly="readonly" class="form-control datetimepicker"> -->
                                <input type="text" name="calendario" class="form-control single-calendar" id="calendario" readonly="readonly"  />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"><i class="icofont-calendar"></i></span>
                                </span>
                            </div>
                            <!-- <input type="text" name="calendario" class="form-control single-calendar" id="calendario" /> -->
                        </div>
                        <div class="col-md-4 form-group">
                            <button type="button" id="btn-buscar" class="btn-success">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mb-2">
                <div class="alert alert-danger alert-dismissible row" id="box-error" style="display:none">
                  <div class="col-lg-1 text-right">
                    <i class="icofont-error"></i>
                  </div>
                  <div class="col-lg-10">
                    <h5 class="font-weight-bold"><i class="icon fas fa-ban"></i> Reserva no realizada!</h5>
                    <p id="detalle-error"></p>
                  </div>
                  <button type="button" class="close">×</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mb-2">
                <div class="alert alert-success alert-dismissible row" id="box-ok" style="display:none">
                  <div class="col-lg-1 text-right">
                    <i class="icofont-check-circled"></i>
                  </div>
                  <div class="col-lg-11">
                    <h5 class="font-weight-bold"><i class="icon fas fa-check"></i> Reserva realizada!</h5>
                    <p>Te enviamos un email con la confirmación del turno.</p>
                  </div>
                  <button type="button" class="close">×</button>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <table id="turnos" class="table" style="width:100%">
              <thead>
                <tr>
                  <th>Turno</th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          </div>
        </div>

      </div>
    </section><!-- End Services Section -->
</main><!-- End #main -->
<!-- MODAL: Reserva de turno -->
<div class="modal fade" id="modal-reserva-turno">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Reserva de turno</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>Completa tu email para confirmar el turno.</p>
            <form id="confirmacion-reserva-form">
              <input type="hidden" id="idTurno" value="" />
              <div class="form-row">
                <label for="datetimepicker" class="col-md-2 col-form-label font-weight-bold">Email:</label>
                <div class="col-md-10 form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="" required/>
                  <div class="invalid-feedback">
                    Complete el campo con un email válido.
                  </div>
                </div>
                
              </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <a href="#" class="btn btn-primary" id="btn-confirmar-reserva">Confirmar</a>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->