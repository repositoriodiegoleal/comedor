<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="<?=Yii::app()->request->baseUrl?>/img/icon.png">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?=Yii::app()->request->baseUrl?>/themes/adminlte/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" >
  <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/css/style.css" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl?>/css/landing.css" />
  <script> var urlSite = '<?=Yii::app()->request->baseUrl?>';</script>
  <script> var verTurnoNoDisponible = '<?=empty(Yii::app()->params['verTurnoNoDisponible'])?'no':'si';?>';</script>
</head>
<body>
	<?php Yii::app()->runController('header/landing'); ?>
  <?php Yii::app()->runController('banners/landing'); ?>
	<?php echo $content; ?>

	<?php Yii::app()->runController('footer/landing'); ?>
  <!-- Vendor JS Files -->
  <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <!-- <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script> -->
  <script src="<?=Yii::app()->request->baseUrl?>/themes/adminlte/plugins/moment/moment.min.js"></script>
  <script src="<?=Yii::app()->request->baseUrl?>/themes/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Template Main JS File -->
  <script src="<?=Yii::app()->request->baseUrl?>/themes/medilab/assets/js/main.js"></script>
  <!-- DataTables  & Plugins -->
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?=Yii::app()->request->baseUrl?>/js/site.js"></script>
</body>
</html>