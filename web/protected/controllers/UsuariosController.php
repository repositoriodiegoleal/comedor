<?php


class UsuariosController extends Controller
{
	public $layout='main_intranet';

	public function filters(){
		return array('accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules(){
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				//'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            )	
        );
	}
	
	public function actionIndex()
	{
		$usuarios = Usuarios::model()->findAll(array('condition'=>'estado!=:estado','params'=>array('estado'=>'E')));
		$permisos = Permisos::model()->findAll();
		$this->render('index', array('usuarios'=>$usuarios, 'permisos'=>$permisos));
	}

	public function actionActualizarEstado()
	{
		if(!empty($_POST['idUsuario'])){
			$usuario = Usuarios::model()->findByPk($_POST['idUsuario']);
			$usuario->estado = $_POST['estado'];
			if($usuario->update()){
				echo json_encode(array(
					"status"=>"ok",
					"msg"=>"Estado actualizado."
					));
			}else{
				echo json_encode(array(
					"status"=>"error",
					"msg"=>"No se pudo actualizar el estado."
					));
			}
		}
	}

	public function actionAgregar()
	{
		$model = new Usuarios;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Usuarios']))
		{//echo '<pre>';print_r($_POST);die;
			$_POST['Usuarios']['estado'] = isset($_POST['Usuarios']['estado']) && $_POST['Usuarios']['estado'] == 'on' ? '1':'0';
			$_POST['Usuarios']['password'] = isset($_POST['Usuarios']['confirmarEmail']) && $_POST['Usuarios']['confirmarEmail'] == 'on' ? Utils::generarToken(6):Yii::app()->params['passwordDefault'];
			$model->attributes=$_POST['Usuarios'];
			if($model->save()){
				if(!empty($_POST['Usuarios']['permisos'])){
					foreach($_POST['Usuarios']['permisos'] as $permiso){
						$usuariopermisos=new Usuariopermisos;
						$usuariopermisos->idusuario=$model->id;
						$usuariopermisos->idpermiso=$permiso;
						$usuariopermisos->save();
					}
				}
				$this->redirect(array('index'));
			}
		}

		$this->render('form',array(
			'model'=>$model,
		));
	}

	public function actionEditar($id)
	{
		$model = Usuarios::model()->findByPk($id);
		if($model->estado == 'E')
			$this->redirect('../../usuarios');
		
		if(isset($_POST['Usuarios']))
		{
			$_POST['Usuarios']['estado'] = isset($_POST['Usuarios']['estado']) && $_POST['Usuarios']['estado'] == 'on' ? '1':'0';
			$model->attributes=$_POST['Usuarios'];
			if($model->save()){
				Usuariopermisos::model()->deleteAll("idusuario=$id");
				if(!empty($_POST['Usuarios']['permisos'])){
					foreach($_POST['Usuarios']['permisos'] as $permiso){
						$usuariopermisos=new Usuariopermisos;
						$usuariopermisos->idusuario=$id;
						$usuariopermisos->idpermiso=$permiso;
						$usuariopermisos->save();
					}
				}
				$this->redirect('../../usuarios');
			}
		}

		// Se recuperan los permisos del usuario.
		$permisosUsuario = array();
		if(!empty($model->permisos)){
			foreach($model->permisos as $permiso){
				array_push($permisosUsuario, $permiso->idpermiso);
			}
		}
		// Se ordenan los permisos de forma ascendente.
		sort($permisosUsuario);

		$this->render('form',array(
			'model'=>$model,
			'permisosUsuario'=>$permisosUsuario,
		));
	}

	public function actionEliminar($id)
	{ 
		// Usuarios::model()->findByPk($id)->delete();
		$model = Usuarios::model()->findByPk($id);
		$model->estado = 'E';
		$model->update();
		$this->redirect('../../usuarios');	
	}

	public function actionResetpassword()
	{
		if(isset($_POST))
		{	
			$usuario = Usuarios::model()->findByPk(Yii::app()->user->id);
			$usuario->password = $_POST['password_1'];
			if($usuario->update()){
				echo json_encode(array(
					"status"=>"ok",
					"msg"=>"Password actualizado."
					));
			}else{
				echo json_encode(array(
					"status"=>"error",
					"msg"=>"No se pudo actualizar el Password."
					));
			}
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}