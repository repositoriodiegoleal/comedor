<?php

class EliminarreservaController extends Controller
{
	public $layout = 'main_control_ingreso_alumno';

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionEliminar($token){
		// $result = Utils::eliminarReservaAlumno($token);
		// echo EliminarreservaController::crearMensaje($result);
		$this->render('index',array('token'=>$token));
	}

	private function crearMensaje($opc){
		$message = "<html>
						<head>
  							<title>".Yii::app()->params['nombreOrigenCorreo']."</title>
						</head>
						<body>
  							<table  style='width: 100%;'>
							  <caption style='background-color: rgb(29, 95, 138);'><h1><font color='White'>".Yii::app()->params['nombreOrigenCorreo']."</font></h1></caption>  
    							<tr>
      								<th>
									    <td><h2>
									  		Contacto de correo: ".Yii::app()->params['emailSbu']."
									    </h2></td>
										<td><h2>
											Contacto de telefono: ".Yii::app()->params['telefonoSbu']."
									    </h2></td>
										<td><h2>
											Dirección: ".Yii::app()->params['direccionSbu']."
									    </h2></td>
									</th>
    							</tr>
    						</table>
							<table  style='width: 100%;'>
								<caption style='background-color: rgb(222, 241, 251);'><h3>".EliminarreservaController::devolverMensaje($opc)."</h3></caption>  
							</table>
						</body>
					</html>";
		return $message;			
	}
	private function devolverMensaje($opc){
		return $opc['message'];
	}

	public function actionCancelar(){
		if(!empty($_POST['token'])){
			$result = Utils::eliminarReservaAlumno($_POST['token']);
			echo json_encode($result);
		}else{
			echo json_encode(array(
				'status'=>'error',
				'message'=>'No se enviaron datos POST',
				'data'=>$_POST
			));
		}
	}
}