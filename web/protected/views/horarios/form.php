<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Editar</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/inicio">Inicio</a></li>
              <li class="breadcrumb-item"><a href="<?=Yii::app()->request->baseUrl?>/horarios">Horarios</a></li>
              <li class="breadcrumb-item active">Editar</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Horario</h3>
                        </div><!-- /.card-header -->
                        <?php 
                            $form=$this->beginWidget('CActiveForm', array(
                                //'action'=>Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/login',
                                //'method'=>'post',
                                'id'=>'horario-form',
                                'enableAjaxValidation'=>false,
                                //'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                            )); 
                        ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success float-right">
                                                <?php //echo $form->checkBox($model,'estado',array('class'=>'custom-control-input', 'id'=>'chk-'.$model->id,'checked'=>'checked')); ?>
                                                <input type="checkbox" class="custom-control-input" name="Horarios[estado]" id="chk-<?=$model->id?>" <?=$model->estado=='1'?'checked':'';?>>
                                                <label class="custom-control-label" for="chk-<?=$model->id?>">Activo</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($model,'dia'); ?>
                                            <?php echo $form->dropDownList($model, 'dia',array('LUN'=>'Lunes','MAR'=>'Martes','MIE'=>'Miércoles','JUE'=>'Jueves','VIE'=>'Viernes','SAB'=>'Sábado','DOM'=>'Domingo'),array('class'=>'form-control '));?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($model,'horainicio'); ?>
                                            <?php echo $form->textField($model,'horainicio',array('class'=>'form-control time-input','autocomplete'=>'off')); ?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($model,'horafin'); ?>
                                            <?php echo $form->textField($model,'horafin',array('class'=>'form-control time-input','autocomplete'=>'off')); ?>
                                            <div class="invalid-feedback">
                                                Complete el campo.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary btn-guardar')); ?>
                                <?php echo CHtml::link('Cancelar', Yii::app()->request->baseUrl.'/horarios' , array('class'=>'btn btn-default')); ?>
                            </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- /.card -->
                </div>
            </div> <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section><!-- /.content -->
</div>
<script>
    var action = '<?=Yii::app()->controller->action->id?>';
</script>