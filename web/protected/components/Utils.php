<?php
//require_once('ean13.php');
require_once('phpmailer.php');
require_once('phpqrlib/qrlib.php');
require_once('fpdf184/fpdf.php');
require_once('class.smtp.php');

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class Utils
{
	
    public static function saludar(){
        echo 'Hola mundo ';
    }

    public static function generarToken($length) {
        $alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $xid = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $xid[] = $alphabet[$n];
        }
        return implode($xid);
    }

    public static function fechaWeb($fecha){
        $fecha = explode('-',$fecha);
        return $fecha[2].'/'.$fecha[1].'/'.$fecha[0];
    }

    public static function fechaBD($fecha){
        $fecha = explode('/',$fecha);
        return $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
    }

    public static function diaWeb($d){
        $days_dias = array(
            'Mon'=>'LUN',
            'Tue'=>'MAR',
            'Wed'=>'MIE',
            'Thu'=>'JUE',
            'Fri'=>'VIE',
            'Sat'=>'SAB',
            'Sun'=>'DOM'
        );
        return $days_dias[$d];
    }

    public static function turnoDisponible($idTurno){
        $turnos = Turnos::model()->findAll(array('condition'=>'id=:idTurno AND estado=:estado','params'=>array('idTurno'=>$idTurno,'estado'=>'1')));
		$reservas = Reservas::model()->findAll(array('condition'=>'idTurno=:idTurno AND estado!=:estado','params'=>array('idTurno'=>$idTurno,'estado'=>'E')));
		return count($reservas) < intval($turnos[0]->cupo);
	}

    public static function reservaPorDia($email, $idTurno){
        $response = true;
        $turnoActual = Turnos::model()->find(array('condition'=>'id=:idTurno AND estado=:estado','params'=>array('idTurno'=>$idTurno,'estado'=>'1')));
        $reservas = Reservas::model()->findAll(array('condition'=>'email=:email AND estado!=:estado','params'=>array('email'=>$email,'estado'=>'E')));
        foreach($reservas as $key => $reserva){
            $turno = Turnos::model()->find(array('condition'=>'id=:idTurno AND estado=:estado','params'=>array('idTurno'=>$reserva->idturno,'estado'=>'1')));
            if(!empty($turno) && !empty($turnoActual) && $turno->fecha == $turnoActual->fecha){
                $response = false;
            }
        }
        return $response;
    }

    public static function reservasPorSemana($email, $idTurno, $limite){
        $reservaSemana = 0;
        $turnoActual = Turnos::model()->find(array('condition'=>'id=:idTurno AND estado=:estado','params'=>array('idTurno'=>$idTurno,'estado'=>'1')));
        if(!empty($turnoActual)){
            $fechaActual = new DateTime($turnoActual->fecha);
            $dia = array('Mon'=>0,'Tue'=>1,'Wed'=>2,'Thu'=>3,'Fri'=>4,'Sat'=>5,'Sun'=>6);
            $indice = $dia[$fechaActual->format("D")];
            
            $semana = array();
            for($i = 0; $i < 7; $i++){
                array_push($semana,date('Y-m-d', strtotime(strval($i-$indice).' days', strtotime($turnoActual->fecha))));
                
            }
            $turnos = Turnos::model()->findAll(array('condition'=>'estado=:estado AND fecha BETWEEN :fechaInicio AND :fechaFin','params'=>array('estado'=>'1','fechaInicio'=>$semana[0],'fechaFin'=>$semana[6])));
            if(!empty($turnos)){
                foreach($turnos as $key => $turno){
                    $reserva = Reservas::model()->find(array('condition'=>'idturno=:idTurno AND email=:email AND estado!=:estado','params'=>array('idTurno'=>$turno->id,'email'=>$email,'estado'=>'E')));
                    if(!empty($reserva)){
                        $reservaSemana+=1;
                    }
                }
            }
        }
        return $reservaSemana < $limite;
    }

    public static function validarEmail($email, $idturno, $tiporeserva, $dni)
    {
        if ($tiporeserva == 'online') {
            $reservas = Reservas::model()->findAll(array('condition' => 'email=:email AND idturno=:idturno', 'params' => array('email' => $email, 'idturno' => $idturno)));
            $querySelect = "SELECT * FROM talumno INNER JOIN tevaluador ON tevaluador.idalumno = talumno.idalumno WHERE correo = '" . $email . "' AND tevaluador.estado='2'";
            $emails = Yii::app()->db->createCommand($querySelect)->queryAll();

            if (count($emails) > 0 && count($reservas) == 0) {
                return Utils::generarPdf($email, $idturno, $tiporeserva, $dni);
            } else if (count($emails) == 0) {
                return array('status' => 'error', 'message' => 'El alumno no tiene la beca aprobada');
            } else if (count($reservas) > 0) {
                return array('status' => 'error', 'message' => 'El alumno ya tiene una reserva');
            }
        } else {
            $dnis = explode(',', $dni);
            $tokens = '';
            foreach ($dnis as $valor) {
                $resultado = Utils::generarPdf($email, $idturno, $tiporeserva, $valor);
                if ($tokens == '') {
                    $tokens = $resultado['data'];
                } else {
                    $tokens = $tokens . ',' . $resultado['data'];
                }
            }
            return array('status' => 'ok', 'data' => $tokens);
        }
    }
    
/**Metodo para crear el pdf**/
    public static function generarPdf($email, $idturno, $tiporeserva, $dni)
    {
        if ($tiporeserva == 'offline') {
            $numero = '09999';
            $apellNombre = 'Alumno/a';
            $facultad = 'Facultad';
        } else {
            $sql = "SELECT apellido,nombre,dni,idalumno FROM talumno WHERE correo='" . $email . "'";
            $result = Yii::app()->db->createCommand($sql)->queryAll();
            $apellNombre = $result[0]['apellido'] . ' ' . $result[0]['nombre'];
            $dni = $result[0]['dni'];
            $idAlumno = $result[0]['idalumno'];
            $sql = "SELECT tfacultad.nombre FROM tfacultad INNER JOIN tacademico ON tfacultad.idfacultad=tacademico.idfacultad INNER JOIN talumno ON tacademico.idalumno=talumno.idalumno WHERE talumno.idalumno='" . $idAlumno . "'";
            $result = Yii::app()->db->createCommand($sql)->queryAll();
            $facultad = $result[0]['nombre'];
            $numero = rand(10000, 99999);
        }
        $sql = "SELECT fecha,idhorario FROM turnos WHERE id='" . $idturno . "'";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $fechaAsistenciaCodigo = str_replace("-", "", $result[0]['fecha']);
        $fechaAsistencia = Utils::convertirfecha($result[0]['fecha']);
        $sql = "SELECT horainicio,horafin FROM horarios WHERE id='" . $result[0]['idhorario'] . "'";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $horaInicio = $result[0]['horainicio'];
        $horaFin = $result[0]['horafin'];

        $codigo = $fechaAsistenciaCodigo . $dni . $numero;
        $pdf = new FPDF('P', 'mm', 'A4');
        $pdf->AddPage();
        if($tiporeserva=='online'){
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->SetXY(10, 10);
            $pdf->Image('./img/logo_comedor.png', 9, 10, 30, 16);
            $pdf->Image('./img/logo_sbu.png', 41, 10, 30, 16);
            $pdf->MultiCell(0, 4, '', 0, 'C', false);
            $pdf->MultiCell(0, 10, 'COMEDOR UNIVERSITARIO', 0, 'C', false);
            $pdf->Image('./img/logo_unju.png', 170, 10, 30, 16);
            $pdf->MultiCell(0, 4, '', 0, 'C', false);
            $pdf->MultiCell(0, 0, '', 1, 'C', false);
            $pdf->SetXY(10, 30);
            $pdf->SetFont('Arial', '', 12);
            $pdf->MultiCell(0, 10, 'Secretaria De Bienestar Universitario', 0, 'C', false);
            $pdf->SetXY(10, 35);
            $pdf->MultiCell(0, 10, 'Universidad Nacional De Jujuy', 0, 'C', false);
            $pdf->MultiCell(0, 2, '', 0, 'C', false);
        }
      	$pdf->SetFont('Arial', '', 12);
        $pdf->MultiCell(0, 0, '', 1, 'C', false);
        $pdf->SetXY(10, 47);
        $pdf->SetFont('Arial', 'B', 15);
        $pdf->MultiCell(0, 10, 'COMPROBANTE DE TURNO', 0, 'C', false);
        //parametros del qr
        $tamaño = 10; //Tamaño de Pixel
        $level = 'L'; //Precisión Baja
        $framSize = 3; //Tamaño en blanco
        //Verificar si existe la carpeta para almacenar temporalmente las constancias
        if (!file_exists(Yii::app()->params['dirGuardarArchTemp'])) {
            mkdir(Yii::app()->params['dirGuardarArchTemp'], 0777, true);
        }
        //Enviamos los parametros a la Función para generar código QR 
        QRcode::png($codigo, Yii::app()->params['dirGuardarArchTemp'] . '/' . $codigo . '.png', $level, $tamaño, $framSize);
        $pdf->Image(Yii::app()->params['dirGuardarArchTemp'] . '/' . $codigo . '.png', 81, 60, 50, 50);
        $pdf->SetXY(10, 107);
        $pdf->MultiCell(0, 2, '', 0, 'C', false);
        $pdf->MultiCell(0, 0, '', 1, 'C', false);
        $pdf->SetXY(10, 112);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->MultiCell(0, 10, 'UTILIZACION', 0, 'C', false);
        /*AQUI VA EL BENEFICIARIO*/
        /*AQUI VA EL DNI */
        /*AQUI VA FACULTAD*/
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(20, 150);
        $pdf->MultiCell(0, 10, 'Fecha de Asistencia:', 0, 'L', false);
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(64, 150);
        $pdf->MultiCell(0, 10, $fechaAsistencia, 0, 'L', false);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(20, 160);
        $pdf->MultiCell(0, 10, 'Horario:', 0, 'L', false);
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(38, 160);
        $pdf->MultiCell(0, 10, $horaInicio . ' a ' . $horaFin . ' Hs.', 0, 'L', false);
        if($tiporeserva=='online'){
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetXY(20, 170);
            $pdf->MultiCell(0, 10, 'Lugar:', 0, 'L', false);
            $pdf->SetFont('Arial', '', 12);
            $pdf->SetXY(35, 170);
            $pdf->MultiCell(0, 10, utf8_decode('Círculo de oficiales de la provincia de Jujuy- Argañaraz N° 347-San Salvador De Jujuy.'), 0, 'L', false);
            $pdf->SetXY(10, 187);
            $pdf->MultiCell(0, 2, '', 0, 'C', false);
            $pdf->MultiCell(0, 0, '', 1, 'C', false);
            $pdf->SetXY(10, 188);
            $pdf->MultiCell(0, 10, utf8_decode('SBU - San Salvador de Jujuy'), 0, 'C', false);
        }
        /*beneficiario Y OTROS DATOS*/
        if($tiporeserva=='online'){
            /*codigo de autoriazacion*/
            $pdf->MultiCell(0, 10, utf8_decode('Codigo de autorización: ' . $codigo), 0, 'C', false);
            //dni
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetXY(20, 130);
            $pdf->MultiCell(0, 10, 'DNI: ', 0, 'L', false);
            $pdf->SetFont('Arial', '', 12);
            $pdf->SetXY(30, 130);
            $pdf->MultiCell(0, 10, $dni, 0, 'L', false);
            //beneficiario
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetXY(20, 120);
            $pdf->MultiCell(0, 10, 'Beneficiario: ', 0, 'L', false);
            $pdf->SetFont('Arial', '', 12);
            $pdf->SetXY(47, 120);
            $pdf->MultiCell(0, 10, utf8_decode($apellNombre), 0, 'L', false);
            //facultad
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetXY(20, 140);
            $pdf->MultiCell(0, 10, 'Facultad: ', 0, 'L', false);
            $pdf->SetFont('Arial', '', 12);
            $pdf->SetXY(40, 140);
            $pdf->MultiCell(0, 10, $facultad, 0, 'L', false);
        }else{
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetXY(20, 130);
            $pdf->MultiCell(0, 10, 'Orden: ', 0, 'L', false);
            $pdf->SetFont('Arial', '', 12);
            $pdf->SetXY(37, 130);
            $pdf->MultiCell(0, 10, $dni, 0, 'L', false);
        } 
        //Guardar en la direccion
        $dir = Yii::app()->params['dirGuardarArchTemp'] . '/' . $codigo . '.pdf';
        unlink(Yii::app()->params['dirGuardarArchTemp'] . '/' . $codigo . '.png'); //Borramos el qr
        $pdf->Output($dir, 'F');
        //Enviar correo
        $resultado = Utils::envioCorreo3($email, Yii::app()->params['asuntoCorreo'], $dir, str_replace("-", "", $fechaAsistencia), Utils::generarBody($codigo));
        if ($resultado) {
            if (unlink($dir)) { //Si llego, entonces borramos el pdf
                $resultado = array('status' => 'ok', 'message' => 'Codigo para verificar la entrada', 'data' => $codigo); //codigo para verificar la entrada
            } else { //No se pudo borrar el pdf
                $resultado = array('status' => 'error', 'message' => 'No se pudo borrar el pdf pero si envio el correo', 'data' => '');
            }
        } else {
            $resultado = array('status' => 'error', 'message' => 'No se pudo enviar el correo', 'data' => $resultado);
        }
        //echo '<pre>';print_r('--------------'.json_encode($resultado));die; 
        return $resultado;
    }
  
public static function envioCorreo2($para,$asunto,$dir=null,$fecha,$body){ 
    $boundary      = "PHP-mixed-".md5(time());
    $boundWithPre  = "\n--".$boundary;
    $headers  = "From: ".Yii::app()->params['emailSbu'];
    //$headers  .= "\nReply-To: ".Yii::app()->params['emailSbu'];
    $headers  .= "\nContent-Type: multipart/mixed; boundary=\"".$boundary."\"";
    
   // $headers = "MIME-Version: 1.0 \r\n";
    //$headers .= "Content-type: text/html; charset=iso-8859-1 \r\n";


    $message   = $boundWithPre;

    $message  .= "\n Content-Type: text/plain; charset=UTF-8\n";
    $message  .= "\n ".$body;
    $message .= $boundWithPre;

    $fileAttachment = trim($dir);
    $attachment    = chunk_split(base64_encode(file_get_contents($fileAttachment)));
    $attchmentName='comprobante'.$fecha.'.pdf';
    $message .= "\nContent-Type: application/octet-stream; name=\"".$attchmentName."\"";
    $message .= "\nContent-Transfer-Encoding: base64\n";
    $message .= "\nContent-Disposition: attachment\n";
    $message .= $attachment;
    $message .= $boundWithPre."--";
    

    // Enviarlo
    //echo '<pre>';print_r(mail($para, 'odilon2022'.$titulo, $message, $headers));die; 
    return mail($para,$asunto, $message, $headers);
}


public static function envioCorreo3($para,$asunto,$dir=null,$fecha,$body){
    $toemail = $para;
    $subject = $asunto;
    $fromemail =  Yii::app()->params['emailSbu'];
    $msg = $body;
    $s_m = md5(uniqid(time()));
    $headers = "From: ".$fromemail;
    $mime_boundary = "==Multipart_Boundary_x{$s_m}x";
    $headers .= "\nMIME-Version: 1.0\n" .
    "Content-Type: multipart/mixed;\n" .
    " boundary=\"{$mime_boundary}\"";
    $file_name = 'documento'.$fecha.'.pdf';
    if($dir!=null){  
        $content = chunk_split(base64_encode(file_get_contents($dir)));  
    }else{
        $content = null;
    }
        $msg .= "This is a multi-part message in MIME format.\n\n" .
            "--{$mime_boundary}\n" .
            "Content-Type:text/html; charset=\"iso-8859-1\"\n" .
            "Content-Transfer-Encoding: 7bit\n\n" .
            $msg .= "\n\n";
    if($dir!=null){        
        $msg .= "--{$mime_boundary}\n" .
            "Content-Type: application/octet-stream;\n" .
            " name=\"{$file_name}\"\n" .
            // "Content-Disposition: attachment;\n" .
            // " filename=\"{$fileatt_name}\"\n" .
            "Content-Transfer-Encoding: base64\n\n".
            $content  .= "\n\n" .
            "--{$mime_boundary}--\n";
    }
    if(mail($toemail, $subject, $msg, $headers)){
        $Msg= "Email send successfully with attachment";
        return true;
    }else{
        $Msg= "Email Not sent";
        return false;
    }
}
















/**Metodo generar body**/
public static function generarBody($codigo){
    $message = "<html>
                    <body>
                        <table>
                            <tr>
                                <td><p><FONT SIZE=4>Estimada/o Estudiante de la UNJu.:</font></p></td>
                            </tr>
                            <tr>
                                <td>
                                    <FONT SIZE=3>Su turno ha sido <b>RESERVADO</b>, por lo tanto, debe <b>ASISTIR</b> y <b>REGISTRAR</b> su asistencia dentro del día y horario seleccionado, por lo que le recomendamos presentarse <b>20 minutos antes</b> de finalizar la franja horaria.</FONT>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <FONT SIZE=3>•  A su ingreso debe mostrar el <b>PDF</b> adjunto a este email (formato digital o impreso).</FONT>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <FONT SIZE=3>•  En el caso de <b>NO</b> podes asistir al turno  Ud. debe <b>eliminar</b> la reserva hasta las horas 12:00 del día anterior al turno mediante el siguiente link:</FONT>
                                    <br><FONT SIZE=3>   ".Yii::app()->params['urlComedor']."/cancelar/reserva/".$codigo."</FONT>
                                <td>
                            </tr>
                            <tr>
                                <td>
                                    <FONT SIZE=3>Saludos cordiales…</FONT>
                                </td>
                            </tr>
                        </table>
                        <br><br>
                        <tablet width='200'>
                            <body width='200'>
                                <tr width='250'>
                                    <td colspan='2'><img src='http://comedor.unju.edu.ar/web/img/logo_unju.png' width='250' height='100'/></td>
                                </tr>
                                <tr width='200'>
                                    <td  width='250' colspan='2'>
                                        <FONT  width='250' SIZE=1 COLOR='gray'>COORDINACIÓN DE COMEDOR UNIVERSITARIO - SBU UNIVERSIDAD NACIONAL DE JUJUY
                                        Av. Bolivia N° 149
                                        San Salvador de Jujuy (CP 4600)<div>
                                        Jujuy – Argentina</div>
                                        </FONT>
                                    </td>
                                </tr>
                                <tr width='250'>
                                    <td><img src='http://comedor.unju.edu.ar/web/img/sbu_.png' width='125' height='50'/></td>
                                    <td><img src='http://comedor.unju.edu.ar/web/img/logo_comedor.png' width='125' height='50'/></td>
                                </tr>
                            </body>
                        </tablet>
                    </body>
                </html>";
    return $message;
}
    /**Metodo cambiar a dd-mm-aaaa */
    private static function convertirfecha($fecha){
        $date = date_create($fecha);
        return date_format($date,"d-m-Y");
    }

    
    /**Metodo para eliminar el registro de reserva*/
    public static function eliminarReservaAlumno($token){
        ini_set('date.timezone', 'America/Argentina/Jujuy');
        $fecha = date('Y').'-'.date('m').'-'.date('d');
        $dataReserva = Reservas::model()->findAll(array('condition'=>'token=:token','params'=>array('token'=>$token)));
        if(count($dataReserva)==1){
            $id= $dataReserva[0]->idturno;
            $dataTurno = Turnos::model()->findAll(array('condition'=>'id=:id','params'=>array('id'=>$id)));
            $fechaTurno= new DateTime($dataTurno[0]->fecha);
            $fechaActual= new DateTime($fecha);
            $diff = $fechaTurno->diff($fechaActual);
            if($fechaTurno<=$fechaActual){
                $result = array('status'=>'error','message'=>'No puede eliminar la reserva.','data'=>'Dia');
            }else{
                if($diff->days>Yii::app()->params['cantidadDiasHabiles']){
                    $result=Yii::app()->db->createCommand()->delete('reservas', 'token=:token', array(':token'=>$token));
                    $result = array('status'=>'ok','message'=>'Se elimino la reserva.','data'=>'');
                }else{
                    if(date("H")<=12){
                        $result=Yii::app()->db->createCommand()->delete('reservas', 'token=:token', array(':token'=>$token));
                        $result = array('status'=>'ok','message'=>'Se elimino la reserva.','data'=>'');
                    }else{
                        $result = array('status'=>'error','message'=>'No puede eliminar la reserva.','data'=>'Hora');
                    }
                }
            }
        }else{
            $result = array('status'=>'error','message'=>'No se encontro la reserva.','data'=>'No existe');
        }
        return $result;
    }

    /**Metodo para controlar si el alumno puede ingresar al comedor*/
    public static function controlIngresoAlumno($tokendni){
        if(strlen($tokendni)==21 || strlen($tokendni)==22){
            $result = Utils::verificarToken($tokendni);
            $result['message'] = "<p>Código ingresado: <span class='dato-entrada'>".$tokendni."</span></p>".$result['message'];
        }else{
            $result = Utils::buscarAlumnoDni($tokendni);
            if($result['status']=='ok'){
                $token = $result['data'];
                $result = Utils::verificarToken($token);
                $result['message'] = "<p>DNI ingresado: <span class='dato-entrada'>".$tokendni."</span></p>".$result['message'];
            }
        }
        return $result;
    }
    
    private static function buscarAlumnoDni($dni){
        ini_set('date.timezone', 'America/Argentina/Jujuy');
        $fechaSistema = date('Y').date('m').date('d');
        $sql = "SELECT token FROM reservas WHERE token like '%".$fechaSistema.$dni."%'";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        if(count($result)>0){
            $result = array('status'=>'ok','message'=>'Tiene una reserva','data'=>$result[0]['token']);
        }else{
            $result = array('status'=>'errorDniSinReserva','message'=>"<p style='color: chocolate;'>El dni ".$dni." no posee reserva.</p>",'data'=>'');
        }
        return $result;
    }

    private static function verificarToken($token){
        $dataReserva = Reservas::model()->findAll(array('condition'=>'token=:token','params'=>array('token'=>$token)));
        if(count($dataReserva)>0){
            if($dataReserva[0]->estado=='0'){
                $dataTurno = Turnos::model()->findAll(array('condition'=>'id=:id','params'=>array('id'=>$dataReserva[0]->idturno)));
                $result = Utils::verificarFecha($dataTurno[0]->fecha);
                if($result['status']=='ok'){
                    $dataHorio= Horarios::model()->findAll(array('condition'=>'id=:id','params'=>array('id'=>$dataTurno[0]->idhorario)));
                    $result = Utils::verificarHora($dataHorio[0]->horainicio,$dataHorio[0]->horafin);
                    if($result['status']=='ok'){
                        BusqReservas::incrementarCantHorario($dataHorio[0]->id);
                        Yii::app()->db->createCommand()->update('reservas',array('estado'=>'1','detalle'=>$dataReserva[0]->detalle.'-'.$result['data']), 'token=:token',array(':token'=>$token));
                        $result['message'] = Utils::buscarDatosAlumnos($token)."<p style='color: forestgreen;'>".$result['message']."</p>";
                    }else{
                        BusqReservas::incrementarCantHorario($dataHorio[0]->id);
                        Yii::app()->db->createCommand()->update('reservas',array('estado'=>'2','detalle'=>$dataReserva[0]->detalle.'-'.$result['data']), 'token=:token',array(':token'=>$token));
                        $result['message']=Utils::buscarDatosAlumnos($token)."<p style='color: chocolate;'>".$result['message']."</p>";
                    }
                }else{
                    if($result['status']=='errorFechaCaducada'){
                        Yii::app()->db->createCommand()->update('reservas',array('estado'=>'3','detalle'=>$dataReserva[0]->detalle.'-'.$result['data']), 'token=:token',array(':token'=>$token));
                    }
                    $result['message']=Utils::buscarDatosAlumnos($token)."<p style='color: chocolate;'>".$result['message']."</p>";
                }
            }else{
                $result = array('status'=>'error','message'=>Utils::buscarDatosAlumnos($token)."<p style='color: chocolate;'>LA RESERVA YA FUE UTILIZADA.</p>",'data'=>'');
            }
        }else{
            $result = array('status'=>'error','message'=>"<p style='color: chocolate;'>NO SE ENCONTRO LA RESERVA.</p>",'data'=>'');
        }
        return $result;
    }
    
    private static function verificarHora($horaInicio,$horaFin){
        ini_set('date.timezone', 'America/Argentina/Jujuy');
        //echo '<pre>';print_r($horaInicio);print_r($horaFin);die; 
        $horaInicio_ = strtotime($horaInicio);
        $horaFin_ = strtotime($horaFin);
        $horaSistema = strtotime(date("H").':'.date("i"));
        if($horaSistema<$horaInicio_){
            return array('status'=>'errorHoraFalta','message'=>'Debe esperar, todavia no es su turno de ingreso. Horario de Ingreso: '.$horaInicio.' hasta las '.$horaFin,'data'=>'');    
        }else{
            if($horaSistema<=$horaFin_){
                return array('status'=>'ok','message'=>'PUEDE INGRESAR AL COMEDOR.','data'=>'Llego bien a su turno. Ingreso a las '.date("H").':'.date("i"));
            }else{
                return array('status'=>'errorHoraCaducada','message'=>'PERDIO SU TURNO DE INGRESO. Debio ingresar desde las '.$horaInicio.' hasta las '.$horaFin,'data'=>'LLEGO TARDE A SU TURNO. Ingreso a las '.date("H").':'.date("i"));    
            }
        } 
    }
    private static function verificarFecha($fechaTurno){
        ini_set('date.timezone', 'America/Argentina/Jujuy');
        $fechaSistema = date('Y').'-'.date('m').'-'.date('d');
        if($fechaSistema==$fechaTurno){
            $result = array('status'=>'ok','message'=>'Es el mismo dia','data'=>'');
        }else{
            $fechaTurno= new DateTime($fechaTurno);
            $fechaActual= new DateTime($fechaSistema);
            if($fechaActual>$fechaTurno){
                $result= array('status'=>'errorFechaCaducada','message'=>'EL COMPROBANTE INDICA QUE PERDIO LA FECHA DE INGRESO. Debio asistir el dia '.Utils::formatoFecha($fechaTurno),'data'=>'Perdio la reserva.');
            }else{
                $result= array('status'=>'errorFechaIncorrecta','message'=>'El comprobante indica que tiene otra fecha para ingresar. FECHA INGRESO: '.Utils::formatoFecha($fechaTurno),'data'=>'');
            }
        }
        return $result;
    }
    /**Metodo para obtener la fecha que se genero el PDF */
    private static function formatoFecha($fecha){
        $fecha = $fecha->format('Y-m-d');
        $fecha = strtotime($fecha);
        
        switch (date('m',$fecha)){
            case 1: $mes = "Enero";break;
            case 2: $mes = "Febrero";break;
            case 3: $mes = "Marzo";break;
            case 4: $mes = "Abril";break;
            case 5: $mes = "Mayo";break;
            case 6: $mes = "Junio";break;
            case 7: $mes = "Julio";break;
            case 8: $mes = "Agosto";break;
            case 9: $mes = "Septiembre";break;
            case 10: $mes = "Octubre";break;
            case 11: $mes = "Noviembre";break;
            case 12: $mes = "Diciembre";break;
            default: $mes= 'YYYY';break;
        }
        return $mes.' '.date('d',$fecha).' del '.date('Y',$fecha);
    }
    /**Metodo buscar datos alumno**/
    private static function buscarDatosAlumnos($token){
        if(strlen($token)==21){
            $dni = substr($token,8,8);
            $onoffline = substr($token,16,5);
        }else{
            $dni = substr($token,8,9);
            $onoffline = substr($token,17,5);
        }
        if($onoffline=='09999'){
            $dataReserva = Reservas::model()->findAll(array('condition'=>'token=:token','params'=>array('token'=>$token)));
            if(count($dataReserva)>0){
                $datos = $dataReserva[0]->detalle.' ';
            }else{
                $datos = 'NO ESTA REGISTRADO EL ALUMNO.';
            }
        }else{
            $sql = "SELECT talumno.apellido,talumno.nombre,talumno.vegetariano,tfacultad.nombre AS nombfacultad FROM talumno ".
                    "INNER JOIN tacademico ON talumno.idalumno=tacademico.idalumno ".
                    "INNER JOIN tfacultad ON tacademico.idfacultad = tfacultad.idfacultad ".
                    "WHERE talumno.dni='".$dni."'";
            $result = Yii::app()->db->createCommand($sql)->queryAll();
            if(count($result)>0){
                if($result[0]['vegetariano']=='1'){
                    $almuerzo = 'ALMUERZO VEGETARIANO';
                }else{
                    $almuerzo = 'ALMUERZO';
                }    
                $datos = 'Alumno/a: '.$result[0]['apellido'].' '.$result[0]['nombre'].' - DNI: '.$dni.' - Asiste a la facultad de '.$result[0]['nombfacultad']. '<br><b><font size="5" color="#078139">'.$almuerzo.'</font></b></br>';
            }else{
                $datos = 'NO ESTA REGISTRADO EL ALUMNO.';
            }
        }
        return $datos;
    }
}