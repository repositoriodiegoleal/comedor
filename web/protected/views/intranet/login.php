<div class="login-box">
	<div class="login-logo">
		<a href="<?= Yii::app()->request->baseUrl; ?>"><b>Comedor</b> Universitario</a>
	</div>
	<?php if(!empty($error)){ ?>
		<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h5><i class="icon fas fa-ban"></i> Error!</h5>
			Usuario o contraseña incorrecta.
		</div>
	<?php } ?>
  	<!-- /.login-logo -->
	<div class="card">
		<div class="card-body login-card-body">
		<p class="login-box-msg">Iniciar sesión</p>
		
		<?php 
			$form=$this->beginWidget('CActiveForm', array(
				//'action'=>Yii::app()->request->baseUrl.'/'.Yii::app()->controller->id.'/login',
				//'method'=>'post',
				'id'=>'login-form',
				'enableAjaxValidation'=>false,
				//'htmlOptions'=>array('enctype'=>'multipart/form-data'),
			)); 
		?>
		<div class="input-group mb-3">
			<?php echo $form->EmailField($model,'username',array('class'=>'form-control', 'placeholder'=>'Usuario','required'=>true)); ?>
			<!-- <input type="email" class="form-control" placeholder="Email"> -->
			<div class="input-group-append">
				<div class="input-group-text">
				<span class="fas fa-envelope"></span>
				</div>
			</div>
			<div class="invalid-feedback">
				Complete el campo con un email válido.
			</div>
		</div>
		<div class="input-group mb-3">
			<?php echo $form->passwordField($model,'password',array('class'=>'form-control', 'placeholder'=>'Contraseña','required'=>true)); ?>
			<div class="input-group-append">
				<div class="input-group-text">
				<span class="fas fa-lock"></span>
				</div>
			</div>
			<div class="invalid-feedback">
				Complete el campo con tu contraseña.
			</div>
		</div>
		<div class="row">
			<div class="col-8">
				<!-- <a href="forgot-password.html">Olvidé mi contraseña</a> -->
			</div>
			<!-- /.col -->
			<div class="col-4">
				<!-- <button type="submit" class="btn btn-primary btn-block">Sign In</button> -->
				<?php echo CHtml::submitButton('Ingresar',array('class'=>'btn btn-primary btn-block btn-login')); ?>
			</div>
			<!-- /.col -->
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/login.js',CClientScript::POS_END) ?> 