<?php

class ControlIngresoAlumnoController extends Controller
{
	public $layout = 'main_control_ingreso_alumno';
	//Valida que no se pueda ingresar directamente a la URL
	public function filters(){
		return array('accessControl', // perform access control for CRUD operations
		);
	}
//Valida que no se pueda ingresar directamente a la URL
	public function accessRules(){
		return array(
			array('deny', //no entra a ninguna accion ningun usuario que no este logueado               
                'users'=>array('?'),
            ),
            array('allow', // acceden todos los usuarios autenticados a todas las acciones
				//'actions'=>Yii::app()->user->getRules(),
				'users'=>array('@'),
            )	
        );
	}

	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionValidar(){
		$model = new ValidarIngreso();
		
		if(isset($_POST['ajax'])&& $_POST['ajax']==='idform'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		$this->render('index',array('model'=>$model));
	}
	public function actionBuscar(){
		//echo 'busqueda';
		$listado = BusqReservas::buscarReservas();
		//echo '<pr>';print_r($listado);die;
		echo json_encode(array(
			"status"=>"ok",
			"msg"=>"Horarios encontrados.",
			"data"=>$listado
		));





	}
}